***************************************************************************************************
***************************************************************************************************
**************************************   FRIDGE - SYSTEM   ****************************************
***************************************************************************************************
***************************************************************************************************
Created by : Pierre-Frédérick DENYS 
Updated : 08/09/2018



Ce dossier contient les codes des deux arduinos nécessaires au fonctionnement de la gâche du frigo.
Les schémas de connexion, et les fichiers eagle des cartes sont dans le dossier "boards". 

L'arduino placé près du raspberry pi et connecté à celui-ci, est utilisé en adaptateur USB/série. 
Il se contente de recevoir le code envoyé par l'arduino du frigo sur la broche 10, et l'envoyer sur /dev/ttyUSB0 de la raspi.
Il renvoie ennsuite la réponse du raspi sur la broche 11 vers l'arduino du frigo. 

L'arduino du frigo permet à l'utilisateur de rentrer son code via le clavier, et commander la gâche du frigo via un relais. 

Pour en savoir plus, consulter le site de la maintenance de l'IE. 