/*
  Software serial multiple serial test

 Receives from the hardware serial, sends to software serial.
 Receives from software serial, sends to hardware serial.

 The circuit:
 * RX is digital pin 10 (connect to TX of other device)
 * TX is digital pin 11 (connect to RX of other device)
 * Resistor of 220 ohms between RESET and 5V and a 10uF condensator between RESET and GND (with the - at RESET).

 modified 09 September 2018
 by Pierre-Frédérick DENYS
 based on Mikal Hart's example

 */
#include <SoftwareSerial.h>
set (active low) 
SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  Serial.println("Sur le port usb");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  mySerial.println("Sur le port software");
}

void loop() { // run over and over
  if (mySerial.available()) {
    Serial.write(mySerial.read());
  }
  if (Serial.available()) {
    mySerial.write(Serial.read());
  }
}
