#include <SoftwareSerial.h>
#include <Keypad.h>

#define LED_WAIT 12
#define LED_ERROR 13
#define LED_OK 11
#define DIR_PIN 9
#define STEP_PIN 10



SoftwareSerial serie =  SoftwareSerial(0, 1);

const byte rows = 4; //four rows
const byte cols = 3; //three columns
char keys[rows][cols] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'C','0','V'}
};
byte rowPins[rows] = {5, 4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[cols] = {8, 7, 6}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, rows, cols );

void setup() {
  
  pinMode(0, INPUT);
  pinMode(1, OUTPUT);
  serie.begin(9600);
  pinMode(LED_WAIT, OUTPUT);
  pinMode(LED_ERROR, OUTPUT);
  pinMode(LED_OK, OUTPUT);
  digitalWrite(12, LOW);
  
  keypad.begin( makeKeymap(keys) );
  keypad.setHoldTime(500);
  pinMode(DIR_PIN, OUTPUT); 
  pinMode(STEP_PIN, OUTPUT); 
  
}

void loop() {  
  unsigned int n = 0;
  char pass[13];
  unsigned int error = 0;
  while (char c = keypad.waitForKey())
  {
    Serial.print(c); 
    if(c == 'V')
    {
      if(n < 4 || n > 12)
      {
        n=0;
        break;
      }
      while(serie.available() > 0) {
          char t = serie.read();
      }
      pass[n] = '\0';
      serie.println(pass);
      
      digitalWrite(LED_WAIT, HIGH);
      while(serie.available() == 0){delay(10);}
      digitalWrite(LED_WAIT, LOW);

      unsigned char val = serie.read(); 
      if(val == '1')
      {
        digitalWrite(LED_OK, HIGH);
        error = 0;
        rotate(138, .01); 
        delay(1000);
        digitalWrite(LED_OK, LOW);
      }  
      else if(val == '2')
      {
        digitalWrite(LED_WAIT, HIGH);
        error = 0;
        rotate(-138 * 37, .01); 
        digitalWrite(LED_WAIT, LOW);
      }
      else
        error++;
      n=0;
      
      if(error >= 3)
        ClignotementErreur(10000);
      else if(error >= 1)
        ClignotementErreur(1500);
    }
    else if(c == 'C')
      n = 0;
    else if(n <= 12)
    {
      pass[n] = c;
      n++;
    }
    
  }


  
}



void ClignotementErreur(unsigned int timeout){
  unsigned int n_iteration = timeout/200;
  for(unsigned int i = 0 ; i < n_iteration ; i++)
  {
    digitalWrite(LED_ERROR, HIGH);
    delay(100);
    digitalWrite(LED_ERROR, LOW); 
    delay(100);
  }
}


void rotate(int steps, float speed){ 
  //rotate a specific number of microsteps (8 microsteps per step) - (negitive for reverse movement)
  //speed is any number from .01 -> 1 with 1 being fastest - Slower is stronger
  int dir = (steps > 0)? HIGH:LOW;
  steps = abs(steps);
  digitalWrite(DIR_PIN,dir); 

  float usDelay = (1/speed) * 70;

  for(int i=0; i < steps; i++){ 
    digitalWrite(STEP_PIN, HIGH); 
    delayMicroseconds(usDelay); 

    digitalWrite(STEP_PIN, LOW); 
    delayMicroseconds(usDelay); 
  } 
} 


