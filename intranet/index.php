<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';

require_once './php/functions.php';
use \IE\DBFactory as DBFactory;

$BDD = DBFactory::createCoffeePDO();
$coffeeData = CoffeeData($BDD);
$coffeeParams = CoffeeParam($BDD);
$soldeCoffee = $coffeeData['sumpaid'] - $coffeeData['sumdue'];
$BDDIE = DBFactory::createServiceAdminPDO();
$apps = array(
  array(
    'name' => 'Owncloud',
    'url' => 'https://owncloud.isenengineering.fr/index.php/login?app=user_cas',
    'img' => 'img/logo_owncloud.png',
    'alt' => 'Le cloud de l\'IE',
    'title' => 'Votre cloud',
    'show' => true
  ),
  array(
    'name' => 'Mail',
    'url' => 'https://mail.isenengineering.fr',
    'img' => 'img/logo_mail.png',
    'alt' => 'Boite Mail',
    'title' => 'Votre boite mail',
    'show' => true
  ),
  array(
    'name' => 'Wiki',
    'url' => 'https://wiki.isenengineering.fr/index.php?title=Sp%C3%A9cial:Connexion&returnto=Accueil',
    'img' => 'img/logo_wiki.png',
    'alt' => 'WikiMedia',
    'title' => 'Engi Wiki',
    'show' => true
  ),
  /*array(
    'name' => 'GitLab',
    'url' => 'https://git.isenengineering.fr',
    'img' => 'img/logo_git.png',
    'alt' => 'GitLab',
    'title' => 'GitLab (deprecated)',
    'show' => true
  ),*/
  array(
    'name' => 'Pastebin',
    'url' => 'https://pastebin.isenengineering.fr',
    'img' => 'img/logo_pastedown.png',
    'alt' => 'Pastebin',
    'title' => 'Partager un bout de code',
    'show' => true
  ),
  array(
    'name' => 'Jira',
    'url' => 'https://jira.isenengineering.fr',
    'img' => 'img/logo_jira.png',
    'alt' => 'Jira atlassian',
    'title' => 'Un gestionnaire de projet Agile',
    'show' => true
  ),
  array(
    'name' => 'Bitbucket',
    'url' => 'https://bitbucket.isenengineering.fr',
    'img' => 'img/logo_bitbucket.png',
    'alt' => 'Bitbucket',
    'title' => 'Gestionnaire de version de l\'IE',
    'show' => true
  ),
  array(
    'name' => 'Kanboard',
    'url' => 'https://kanboard.isenengineering.fr',
    'img' => 'img/logo_kanboard.png',
    'alt' => 'Logo de Kanboard',
    'title' => 'Gestionnaire de projet (deprecated)',
    'show' => true,
    'deprecated' => true,
  ),
  array(
    'name' => 'La Famille',
    'url' => '#',
    'img' => 'img/logo_list.png',
    'alt' => 'Membres',
    'title' => 'Liste des membres de l\'IE',
    'show' => true,
    'data-toggle' => 'modal',
    'data-target' => '#modalListe',
    'onclick' => 'refreshList(\'\', this);'
  ),
  array(
    'name' => 'Publier',
    'url' => '#',
    'img' => 'img/logo_blog.png',
    'alt' => 'Blog',
    'title' => 'Poster un article sur notre blog',
    'show' => count(array_intersect(ListeGroupes()[phpCAS::getUser()], BlogsCN($BDDIE))) > 0,
    'data-toggle' => 'modal',
    'data-target' => '#modalBlog',
    'onclick' => ''
  ),
  array(
    'name' => 'FabLab',
    'url' => '#',
    'img' => 'img/logo_fablab.png',
    'alt' => 'Demandes',
    'title' => 'Liste des demandes du fab lab',
    'show' => $IS_ADMIN || $IS_UPKEEP || in_array('isenopensource', ListeGroupes()[phpCAS::getUser()]) || in_array('bureau', ListeGroupes()[phpCAS::getUser()]),
    'data-toggle' => 'modal',
    'data-target' => '#modalFablab',
    'onclick' => 'refreshFablab();'
  ),
  /*array(
    'name' => 'Odoo',
    'url' => 'https://odoo.isenengineering.fr',
    'img' => 'img/logo_odoo.png',
    'alt' => 'Odoo',
    'title' => 'Gestion de stock',
    'show' => $IS_ADMIN || $IS_UPKEEP
  ),*/
  array(
    'name' => 'Horus',
    'url' => 'https://horus.isenengineering.fr',
    'img' => 'img/logo_horus.png',
    'alt' => 'Sécurité',
    'title' => 'Log OSSEC',
    'show' => $IS_ADMIN
  ),
  array(
    'name' => 'Aide',
    'url' => 'https://intranet.isenengineering.fr/aide_dsi.php',
    'img' => 'img/aide.png',
    'alt' => 'Aide',
    'title' => 'Aide DSI',
    'show' => $IS_ADMIN
  ),
  /*array(
    'name' => 'LDAP',
    'url' => 'https://ldap.isenengineering.fr/ui/html',
    'img' => 'img/logo_ldap.png',
    'alt' => 'OpenLDAP',
    'title' => 'Gestion du LDAP',
    'show' => $IS_ADMIN
  ),*/
  array(
    'name' => 'Admin',
    'url' => 'https://ldap.isenengineering.fr/admin',
    'img' => 'img/logo_admin.png',
    'alt' => 'Admin',
    'title' => 'Pages d\'administration',
    'show' => $IS_ADMIN || $IS_UPKEEP
  ),
  array(
    'name' => 'Duo security',
    'url' => 'https://admin.duosecurity.com/',
    'img' => 'img/logo_duo.png',
    'alt' => 'Duo security',
    'title' => 'Pages d\'administration DUO',
    'show' => $IS_ADMIN
  ),
  array(
    'name' => 'Coffee achievement',
    'url' => './coffee/loginCAS.php',
    'img' => 'img/logo_coffee.png',
    'alt' => 'Coffee',
    'title' => 'Votre compte café',
    'show' => true
  )
);
?>




<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<title>Panel Personnel IE</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<link href="css/styleshfnet.css" rel="stylesheet" type="text/css" media="all"/>
<link href="css/fablab.css" rel="stylesheet" type="text/css" media="all"/>


</head>
<body ondragstart="return false;">
	<!--          header             -->


    <div class="container">

          <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="https://isenengineering.fr"><img src ="img/logo_ie.png" alt="Logo IE" title ="logo_ie" width="45" class="img-responsive center-block" /></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="">Menu</a></li>
              <li><a href="#" data-toggle="modal" data-target="#modalCoffee" title="Modifier votre code">Cafés</a></li>
              <li><a href="./aide.php">Aide</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right" >
              <li><a href="#" data-toggle="modal" data-target="#modalCoffee" title="Modifier votre code"><?php if($coffeeData == null) echo '<img src="./img/icon-coffee.png" width="16px" alt="icon-coffee"/> S\'inscrire'; elseif($coffeeData['blocked']) echo '  <span style="color: red;"><span class="glyphicon glyphicon-lock"></span>  '.number_format($soldeCoffee,2, '.', '') .' €</span>'; else echo '<img src="./img/icon-coffee.png" width="16px" alt="icon-coffee"/>  '.number_format($soldeCoffee,2, '.', '').'€'; ?></a></li>
              <li><a href="#">Bienvenue <?php echo ucwords(str_replace('.', ' ', phpCAS::getUser())); ?></a></li>
              <li><a href="?logout=">Déconnexion</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron text-center">
        <h2>Mes applications</h2>

	<div class="applications">


       <?php
	if(isset($_GET['error']))
	{
	?>

    <div class="alert alert-danger alert-dismissible fade in" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">×</span>
       </button>
       <strong>OOuuuups</strong> <br/><?php echo filter_input(INPUT_GET, 'error', FILTER_SANITIZE_STRING); ?>
    </div>
	<?php
	}


	echo "<div class=\"row\">\n";
	$i = 0;
       foreach($apps as $app)
       {
           if(!$app['show'])
               continue;
           $i++;

           if($app['url'] != '#')
               $a_tag = '<a target="_blank" href="'.$app['url'].'">';
           else
               $a_tag = '<a href="'.$app['url'].'" data-toggle="'.$app['data-toggle'].'" data-target="'.$app['data-target'].'" onclick="'.$app['onclick'].'">';
           echo '
                <div class="col-sm-3 text-center ie-app'.(isset($app['deprecated']) ? ' deprecated-app' : '').'">
                '.$a_tag.'
                    <img src="'.$app['img'].'" alt="'.$app['alt'].'" title="'.$app['title'].'" class="img-responsive center-block img-rounded ie-app-img"/>
                </a>
                <p class="ie-app-text'.(isset($app['deprecated']) ? '  text-warning"' : '').'">'.$app['name'].'</p>'.
               "</div>\n";
           if($i == 4)
           {
               echo "</div>\n<div class=\"row\">\n";
               $i=0;
           }

       }

	echo "</div>\n";

	?>




	</div>
      </div>
    </div> <!-- /container -->


<div class="modal fade" id="modalCoffee" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="modalLabel">Cafés</h2>
      </div>
	<form class="form-inline formCoffee" method="POST" action="php/updateCoffeeUser.php">
		<div class="modal-body modalBodyCoffee">
		<?php if($coffeeData['blocked']) echo '<h4><span style="color: red;"><span class="glyphicon glyphicon-lock"></span>Compte bloqué.</span></h4>'; ?>
		<h4>Solde : <?php echo number_format($soldeCoffee,2, '.', '') . '€'; ?></h4>
	        <input type="password" class="form-control" minlength="4" maxlength="12" name="code" placeholder="Nouveau code (4 à 12 chiffres)" required/>
      		</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        <button type="submit" class="btn btn-primary">Sauvegarder</button>
      </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modalListe" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title">👪 La Famille</h3>
      </div>
	<div class="modal-body clearfix" id="modalBodyListe">

		<ul class="nav nav-tabs table-responsive nav-listIE">
			<li role="presentation" id="main-li" class="active"><a href="#" onclick="refreshList('', this);" >Tous</a></li>
			<li class="dropdown" ><a class="dropdown-toggle" data-toggle="dropdown" href="#">Clubs <span class="caret"></span></a>
				<ul class="dropdown-menu">
				<?php
				$groupes = ListeGUID();
				foreach($groupes as $groupe)
					echo '<li role="presentation"><a href="#" onclick="refreshList(\''.$groupe['cn'].'\', this);">'.ucwords(str_replace('isen', '', $groupe['cn'])).'</a></li>';
				?>
				</ul>
			</li>
			<li role="presentation" ><a href="#" onclick="refreshList('anciens', this);" >Anciens</a></li>
		</ul>
		<div id="containerListe">
			Loading ...
		</div>

      	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalFablab" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title">Liste des demandes</h3>
      </div>
	<div class="modal-body modalBodyFablab">
	    Loading ...
	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
        </form>
    </div>
  </div>
</div>


<div class="modal fade" id="modalBlog" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3 class="modal-title">Publier</h3>
      </div>
	<div class="modal-body">
	    <ul class="nav nav-tabs" role="tablist">
	       <li role="presentation" class="active"><a href="#blogNew" aria-controls="blogNew" role="tab" data-toggle="tab">Créer</a></li>
	       <li role="presentation"><a href="#blogEdit" aria-controls="blogEdit" role="tab" data-toggle="tab">Editer</a></li>
	    </ul>
	    <div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="blogNew">
        <form class="form-horizontal" action="php/addBlog.php" method="POST">
   		<div class="form-group">
    		    <label for="inputClub" class="col-sm-2 control-label">Blog</label>
   		     <div class="col-sm-10">
			<select name="club" class="form-control" id="inputClub">
			    <option selected disabled>Sélectionner un club</option>
				<?php
				$groupes = ListeGroupes()[phpCAS::getUser()];
				foreach($groupes as $group)
				{
                                    $blog = BlogClub($BDDIE, $group);
				    if($blog != null)
				    	echo '<option value="'.$group.'">'.$blog['name'].'</option>';
				}
				?>
			</select>
    		    </div>
  		</div>

  	    	<div class="form-group">
    		    <label for="inputTitle" class="col-sm-2 control-label">Titre</label>
   		     <div class="col-sm-10">
      			<input type="text" name="title" class="form-control" id="inputTitre" placeholder="Titre">
    		    </div>
  		</div>
  		<div class="form-group">
    		    <label for="inputSubTitle" class="col-sm-2 control-label">Accroche</label>
    		    <div class="col-sm-10">
      			<input type="text" name="subtitle" class="form-control" id="inputSubTitle" placeholder="Accroche">
    		    </div>
  		</div>
		<div class="form-group">
    		    <label class="col-sm-2 control-label"></label>
    		    <div class="col-sm-10">
		    	<textarea class="form-control" name="editor"></textarea>
		    </div>
		</div>
	    <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
            <script>
                CKEDITOR.replace( 'editor' );
		$(document).ready(function(){

$.fn.modal.Constructor.prototype.enforceFocus = function () {
    modal_this = this
    $(document).on('focusin.modal', function (e) {
        if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
        // add whatever conditions you need here:
        &&
        !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
            modal_this.$element.focus()
        }
    })
};

});
            </script>

	<div class="form-group">
    		    <label class="col-sm-2 control-label">Visible</label>
    		    <div class="col-sm-1">
		    	<input type="checkbox" class="form-control" name="publish" />
		    </div>
	</div>
	<div class="form-group">
    		    <div class="col-sm-2 col-sm-offset-2">
        <button type="submit" class="btn btn-primary" >Enregistrer</button>
		    </div>
	</div>
      </form>
	</div>
	<div role="tabpane" class="tab-pane table-responsive" id="blogEdit">
	    <table class="table table-striped">
		<thead>
		    <th>Titre</th>
		    <th>Auteur</th>
		    <th>Editer</th>
		    <th></th>
		</thead>
<?php
		foreach($groupes as $group)
		{
			$articles = BlogArticles($BDDIE, $group);
			foreach($articles as $article)
			{
					echo '<tr '.(!$article['published'] ? 'class="warning"' : '') .'><td>'.$article['title'].'</td><td>'.$article['author'].'</td><td><a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-pencil" onClick="editBlog('.$article['id'].')"></span></a></td><td><a class="btn btn-link btn-sm" target="_blank" href="https://blog.isenengineering.fr/blog/'.$article['id'].'">Lien</a></td></tr>';
			}

		}
?>
</table>

	</div>
	</div>
      	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function refreshList(group, element)
{
	$( "#containerListe" ).html( "Loading ..." );
	$.get( "liste.php?group="+group, function( data ) {
	  $( "#containerListe" ).html( data );
	});
	if($(element).parent().is('li'))
	{
		$('.nav-listIE li.active').removeClass('active');
		$(element).parent().addClass('active');
	}
	else
	{
		$('.nav-listIE li.active').removeClass('active');
		$('#main-li').addClass('active');
	}

}
function refreshFablab(id)
{
	if(id == undefined)
		id='';
	$( ".modalBodyFablab" ).html( "Loading ..." );
	$.get( "demandeFablab.php?id="+id, function( data ) {
	  $( ".modalBodyFablab" ).html( data );
	  $('[data-toggle="popover"]').popover();
	});

}

$(".deprecated-app").click(function (){
    alert("Cette application va bientôt être supprimée.\nElle ne devrait plus être utilisée.")
});

<?php
	if(isset($_GET['coffee']))
		echo '$("#modalCoffee").modal("show");';
?>
</script>


</body>
</html>
