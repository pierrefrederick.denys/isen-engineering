<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Panel Personnel IE</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="noindex,nofollow">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <!--<script type="text/javascript" src='js/inscription.js'></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
      integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
      integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->

<link href="css/styleshfnet.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>
<!--          header             -->
<div class="container">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="https://isenengineering.fr"><img src="img/logo_ie.png" alt="Logo IE"
                                                                               title="logo_ie" width="45"
                                                                               class="img-responsive center-block"/></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="./index.php">Menu</a></li>
                    <li class="active"><a href="">Aide</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div>
        </div>
    </nav>

    <!-- CONTENU -->

    <div class="contenu">
        <h1 class="text-center">Aide</h1>

        <div class="container">
            <div id="rty" data-spy="scroll" data-target="#myScrollspy" data-offset="20">

                <div class="container">
                    <div class="row">
                        <nav class="col-sm-2" id="myScrollspy">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#generalites">Généralités</a></li>

                                <li class="services">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#services">Services<span
                                                class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#owncloud">Owncloud</a></li>
                                        <li><a href="#mail">Mail</a></li>
                                        <li><a href="#wiki">Wiki</a></li>
                                        <li><a href="#pastebin">Pastebin</a></li>
                                        <li><a href="#jira">Jira</a></li>
                                        <li><a href="#bitbucket">Bitbucket</a></li>
                                        <li><a href="#kanboard">Kanboard</a></li>
                                        <li><a href="#lafamille">La Famille</a></li>
                                        <li><a href="#blog">Publier sur le Blog</a></li>
                                        <li><a href="#fablab">FabLab (accès limité)</a></li>
                                    </ul>
                                </li>
                                <li><a href="#cafe">Café</a></li>
                                <li><a href="#assistance">Assistance</a></li>
                            </ul>
                        </nav>

                        <div class="col-sm-9">
                            <div id="generalites">
                                <h2>Généralités</h2>
                                <div class="alert alert-success" role="alert">
                                    <p><-- Découvrez les différents services de l'intranet en naviguant sur cette page avec le menu</p>
                                </div>
                                <p>L’ensemble du service informatique de l’association est accessible sur la plate-forme
                                    : </br>
                                    <a href="http://intranet.isenengineering.fr" target="_blank">http://intranet.isenengineering.fr</a></br>
                                    Cette plate-forme est entretenue et améliorée par le DSI de l'association. En cas de
                                    problème, consulter ce document d’aide ou contacter l’administrateur.
                                    Après avoir complété le formulaire d’inscription en début d'année, vous avez défini
                                    un <b>mot de passe</b>. Afin de se connecter au serveur, entrer votre login sous la
                                    forme <em>prenom.nom</em> et le mot de passe que vous avez défini.
                                </p>

                                <h3>Connexion</h3>
                                <img src="img/aide/generalites/connexion.png" alt="Logo Owncloud" title="logo_owncloud"
                                     width="600" class="img-responsive center-block"/>
                                <h3>Panel utilisateur</h3>
                                <img src="img/aide/generalites/panel.png" alt="Logo Owncloud" title="logo_owncloud"
                                     width="600" class="img-responsive center-block"/>
                                <br>
                                <p>Suivant votre rôle dans l'association, vous avez accès à nombre de services défini
                                    par le CA.</p>

                            </div>
                            <h2>Services</h2>
                            <div id="owncloud">
                                <h3>Owncloud</h3>
                                <p>Owncloud est utilisé pour la <b>gestion de documents</b> et la <b>gestion des
                                        plannings</b>. </p>
                                <img src="img/aide/owncloud/connexion.png" alt="connexion" title="connexion" width="200"
                                     class="img-responsive center-block"/>
                                <p>Vous pouvez ajouter des fichiers ou les modifier via l'interface web, ou via
                                    l'application de synchronisation (comme Dropbox). </p>
                                <h5>Installer l'application de synchronisation :</h5>
                                <p>Un service cloud similaire à Dropbox vous est offert. Vous disposez de 1 GB d’espace
                                    disque. Ce quota est extensible sur demande. </p>
                                <p>Télécharger le logiciel de synchronisation correspondant à votre architecture :</p>
                                <img src="img/aide/owncloud/appli.png" alt="appli" title="appli" width="600"
                                     class="img-responsive center-block"/>
                                <p>Entrer l'adresse du serveur :</p>
                                <img src="img/aide/owncloud/appli_1.png" alt="appli_1" title="appli_1" width="300"
                                     class="img-responsive center-block"/>
                                <p>Entrer vos identifiants :</p>
                                <img src="img/aide/owncloud/appli_2.png" alt="appli_2" title="appli_2" width="300"
                                     class="img-responsive center-block"/>
                                <p>Enfin, vous pouvez choisir de synchroniser tous les dossiers de votre clubs ou
                                    seulement une partie vous concernant. Vous pouvez également choisir le dossier où
                                    synchroniser les fichiers : </p>
                                <img src="img/aide/owncloud/appli_3.png" alt="appli_3" title="appli_3" width="300"
                                     class="img-responsive center-block"/>
                                <p> Si la connexion a réussie, un dossier est créée à l’emplacement choisi, et la
                                    synchronisation débute. </p>
                                <img src="img/aide/owncloud/appli_4.png" alt="appli_4" title="appli_4" width="90"
                                     class="img-responsive center-block"/>
                                <p><em> Pour configurer le proxy à l’ISEN :</em> cliquer sur l’icône dans la barre des
                                    tâches : </p>
                                <img src="img/aide/owncloud/appli_5.png" alt="appli_5" title="appli_5" width="300"
                                     class="img-responsive center-block"/>
                                <p> Dans l’onglet réseau, choisir utiliser les paramètres du système. </p>
                                <img src="img/aide/owncloud/appli_6.png" alt="appli_6" title="appli_6" width="500"
                                     class="img-responsive center-block"/>

                                <h4>Partage de fichiers</h4>
                                <p>Il est possible de partager des dossiers entre les membres d'un groupes, un
                                    utilisateur unique ou par lien public sur l'interface web. </br>
                                    Uniquement le partage public est disponible dans l'explorateur de fichier sur
                                    PC.</p>
                                <img src="img/aide/owncloud/partage_1.png" alt="partage_1" title="partage_1" width="500"
                                     class="img-responsive center-block"/>
                                <p>Uniquement le partage public est disponible dans l'explorateur de fichier sur
                                PC.</p>
                                <img src="img/aide/owncloud/partage_2.png" alt="partage_2" title="partage_2" width="200"
                                     class="img-responsive center-block"/>
                                <h4>Calendrier</h4>
                                <p>Le calendrier partagé de l’association est géré par l’application owncloud et est
                                    accessible dans le menu : </p>
                                <img src="img/aide/owncloud/agenda.png" alt="calendar_1" title="calendar_1"
                                     width="900" class="img-responsive center-block"/>
                                <br>
                                <div class="alert alert-success" role="alert">
                                    <p>De nombreuses options sont disponibles tels que l'export .ics pour voir l'agenda sur votre téléphone, ajouter un calendrier par club ou par équipe
                                        <a href="#assistance"><b>(Contacter le DSI)</b></a></p>
                                </div>

                            </div>

                            <div id="mail">
                                <h3>Mail</h3>
                                <p> Une adresse E-mail de la forme prenom.nom@isenengineering.fr est attribuée à tous les membres de l'association qui se sont inscrits sur l'intranet.</p>
                                <p>Vous pouvez y accéder via l'interface web roundcube disponible sur l'accueil de l'intranet. <b>Le CAS n'est pas intégré sur cette application, il faut donc se
                                logguer une seconde fois.</b> </p>
                                <img src="img/aide/mail/mail.png" alt="mail" title="mail" width="900" class="img-responsive center-block"/>
                                <br>
                                <div class="alert alert-success" role="alert">
                                    <p>Vous pouvez rediriger vos mails vers une adresse e-mail personnelle ou sur votre téléphone. <a href="#assistance"><b>(Contacter le DSI)</b></a> </p>
                                    <p>Vous pouvez également obtenir des alias de la forme alias@isenengineering.fr pour un projet par exemple. <a href="#assistance"><b>(Contacter le DSI)</b></a></p>
                                </div>

                            </div>

                            <div id="wiki">
                                <h3>Wiki</h3>
                                <p>L'application wiki permet de mettre en ligne des tutoriels et de les partager.</p>
                                <p>Tous les membres de l'IE peuvent contribuer en écrivant des article sur le wiki !.</p>
                                <p> <b>Pour vous connecter, cliquer sur connexion :</b></p>
                                <img src="img/aide/wiki/connexion.png" alt="connexion" title="connexion" width="600"
                                     class="img-responsive center-block"/>
                                <p>Vous pouvez consulter l'aide en ligne du logiciel <a
                                            href="https://meta.wikimedia.org/wiki/Help:Contents/fr"> <b>ICI</b></a> pour apprendre la syntaxe wiki.</p>
                                <br>
                                <div class="alert alert-warning" role="alert">
                                    <p>La structure de la page d'accueil est à respecter, bien penser à ajouter un lien si une nouvelle page est créée, et mettre à jour le statut de rédaction des articles. </p>
                                </div>
                            </div>

                            <div id="pastebin">
                                <h3>Pastebin</h3>
                                <p>L'application pastebin est basée sur le logiciel opensource stikked.</p>
                                <p>L'application permet de <b>partager du code facilement, et de manière sécurisé si l'option encrypted est choisie</b>.</p>
                                <p>Il supporte la coloration syntaxique et il suffit de copier-coller votre code puis de générer le lien et de l'envoyer à vos destinataires !</p>
                                <img src="img/aide/pastebin/pastebin.png" alt="connexion" title="connexion" width="600"
                                     class="img-responsive center-block"/>
                            </div>

                            <div id="jira">
                                <h3>Jira</h3>
                                <p>Jira est <b>un puissant logiciel de gestion de projet</b>, il permet de gérer les tâches (comme trello) de les lier à un gestionnaire de version tel que git
                                ou bitbucket.</p>
                                <p>Il est destiné à gérer des projets importants, surtout en développement informatique.</p>
                                <p>Vous pouvez installer l'application <a href="https://itunes.apple.com/us/app/jira-mobile-enterprise/id1073998767?mt=8">Iphone</a> ou
                                    <a href="https://play.google.com/store/apps/details?id=com.infosysta.mobile.jira">Android</a> pour suivre votre projet depuis votre smartphone.</p>

                                <br>
                                <div class="alert alert-success" role="alert">
                                <b>Tous les membres de l'association y ont accès, et il est possible de l'utiliser pour les projets ISEN (d'autres étudiants hors IE
                                    peuvent se connecter). </b> <br>
                                    N'hésiter pas à l'utiliser pour vos projets d'algo, projets techniques, il est important de savoir l'utiliser car fortement utilisé en entreprise.<br>
                                    Pour obtenir la création d'un projet et ajouter des membres hors IE, <a href="#assistance"><b>(Contacter le DSI)</b></a>.
                                </div>
                                <img src="img/aide/jira/jira.png" alt="jira" title="jira" width="600" class="img-responsive center-block"/>
                                <p><b>Une license nous a gracieusement été accordée par Atlassian, qui coûte normalement plusieurs milliers d'euros !!</b></p>
                            </div>

                            <div id="bitbucket">
                                <h3>Bitbucket</h3>
                                <p>L'application Bitbucket est un client GUI du <b> gestionnaire de version Bitbucket (basé sur Git)</b>, il est intégré sur notre système à Jira, ce qui permet une gestion de projet facilitée.</p>
                                <p>Il est destiné à gérer des projets importants, surtout en développement informatique.</p>
                                <p>Vous pouvez installer l'application <a href="https://itunes.apple.com/us/app/jira-mobile-enterprise/id1073998767?mt=8">Iphone</a> ou
                                    <a href="https://play.google.com/store/apps/details?id=com.infosysta.mobile.jira">Android</a> pour suivre votre projet depuis votre smartphone.</p>

                                <br>
                                <div class="alert alert-success" role="alert">
                                    <b>Tous les membres de l'association y ont accès, et il est possible de l'utiliser pour les projets ISEN (d'autres étudiants hors IE
                                        peuvent se connecter). </b> <br>
                                    N'hésiter pas à l'utiliser pour vos projets d'algo, projets techniques, il est important de savoir l'utiliser car fortement utilisé en entreprise.<br>
                                    Pour obtenir la création d'un projet et ajouter des membres hors IE, <a href="#assistance"><b>(Contacter le DSI)</b></a>.
                                </div>
                                <img src="img/aide/bitbucket/bitbucket.png" alt="bitbucket" title="bitbucket" width="600" class="img-responsive center-block"/>
                            </div>

                            <div id="kanboard">
                                <h3>Kanboard</h3>
                                <p>Kanboard est <b>logiciel de gestion de projet</b> (comme Jira), beaucoup plus facile à utiliser et adapté à des projets d'informatique ou d'électronique.</p>
                                <p>Il permet une gestion de projet facilitée, avec la répartition des tâches entre les utilisateurs...</p>

                                <img src="img/aide/kanboard/kanboard.png" alt="bitbucket" title="bitbucket" width="600" class="img-responsive center-block"/>
                                <p>Pour ajouter un nouveau projet, cliquer sur le + : </p>
                                <img src="img/aide/kanboard/nouveau.png" alt="bitbucket" title="bitbucket" width="300" class="img-responsive center-block"/>
                            </div>

                            <div id="lafamille">
                                <h3>La Famille !!</h3>
                                <p>La grande famille de l'IE. <b>Si vous chercher la liste complète des membres, qui est dans quel club, l'adresse d'un membre... c'est içi !</b></p>

                                <img src="img/aide/famille/famille.png" alt="famille" title="famille" width="600" class="img-responsive center-block"/>
                                <p>Les chefs d'équipes et de clubs sont surlignés en vert. </p>
                            </div>

                            <div id="blog">
                                <h3>Le Blog</h3>
                                <img src="img/aide/blog/blog_show.png" alt="blog" title="blog" width="600" class="img-responsive center-block"/>
                                <p>L'IE possède un blog où des articles peuvent être postés par tous les membres. Les articles sont classés par clubs.
                                    <a target="_blank" class="btn btn-success" href="https://blog.isenengineering.fr/">Lien vers le blog</a></p>
                                <p>Un membre ne peut poster un article qu'en rapport avec son club. Pour demander l'activation de l'editeur <a href="#assistance"><b>(Contacter le DSI)</b></a></p>


                                <img src="img/aide/blog/insert_blog.png" alt="blog" title="insert_blog" width="600" class="img-responsive center-block"/>
                            </div>

                            <div id="fablab">
                                <h3>Fablab</h3>
                                <img src="img/aide/fablab/fablab.png" alt="fablab" title="fablab" width="600" class="img-responsive center-block"/>
                                <p>Application accesssible uniquement si vous êtes un membre du club Fablab. Il permet de gérer les demandes d'aides envoyées via le site :
                                    <a target="_blank" class="btn btn-success" href="https://opensource.isenengineering.fr/fablab/">Fablab</a></p>
                                <p>Vous pouvez cliquer sur "plus" pour lire le message du client, et obtenir son e-mail. Vous pouvez récupérer le fichier envoyé avec "fichier" (attention,
                                formulaire public, donc fichiers potentiellement dangeureux). Vous pouvez ensuite cliquer sur "coche" pour indiquer que cette demande est traitée, et la faire
                                disparaître.</p>
                            </div>



                            <div id="cafe">
                                <h2>Café</h2>
                                <p>Non vous ne rêvez pas ! Etre membre de l’IE vous donne la possibilité de bénéficier
                                    de café pour une <b>modique somme de 0.14€ !</b><br>
                                    Pour cela, contactez un membre du bureau et donnez-lui un acompte qui vous servira
                                    de crédit à chaque utilisation.<br>
                                <h3>Tu est membre de l'IE ?</h3>
                                <p> Vous pourrez alors consulter vos crédits restant dans le bandeau en haut à droite de
                                    la page d’accueil de l’intranet, à côté de l’icône représentant une tasse de
                                    café.<br>
                                    Si vous cliquez en dans le bandeau en haut à gauche sur « Cafés », un pop-up
                                    s’ouvrira et vous permettra de mettre à jour votre code. Ce code servira à récupérer
                                    vos dosettes au distributeur de dosette situé dans les locaux de l’IE en 556.
                                </p>
                                <img src="img/aide/cafe/cafe.png" alt="cafe" title="café"
                                     class="img-responsive center-block"/>

                                <h3>Inscrire un étudiant ISEN qui n'est pas à l'IE</h3>
                                <p>Il suffit de faire compléter le formulaire suivant par l'étudiant ou le prof avec ses
                                    identifiants ISEN et choisir un code de son choix.</p>
                                <div class="text-center"><a href="https://intranet.isenengineering.fr/extCoffee/"
                                                            target="_blank" class="btn btn-success">Lien</a> <br> <br>
                                </div>
                                <img src="img/aide/cafe/cafe_ext.png" alt="cafe_ext" title="café externe"
                                     class="img-responsive center-block"/>
                            </div>


                            <div id="assistance">
                                <h2>Assistance</h2>
                                <b>Vous pouvez contacter l'administrateur via les adresses mails suivantes : </b>
                                <ul>
                                    <li><b>admin@isenengineering.fr :</b> pour tout ce qui concerne l'accessibilité aux
                                        services, les défaults.
                                    </li>
                                    <li><b>webmaster@isenengineering.fr :</b> pour les demandes concernants les
                                        différents sites web de l'association
                                    </li>
                                </ul>
                                <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> <!-- /container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</body>
</html>


-->