<?php

if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
{
    require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
}
else
{
    require_once '/var/www/phpCAS/CAS/mustBeCoffeLogged.php';
}
require_once './../php/functions.php';
use \IE\DBFactory as DBFactory;

$db = DBFactory::createCoffeePDO();
$achievementID = filter_input ( INPUT_GET , "id", FILTER_SANITIZE_NUMBER_INT);
$username = phpCAS::getUser();
$coffeeData = CoffeeData($db); 

?>
<!DOCTYPE html>
<html>
<head>
    <title>Achievements</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-3.3.0.js"></script>
    <script src="js/popper_1.12.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/customized/ui.css">
    <link rel="stylesheet" href="css/customized/achievements.css">
    <link rel="stylesheet" href="css/customized/responsive.css">
</head>
<body>
<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group noMobile" href="index.php">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title noMobile">Coffee Achievements</p>
        </a>
        <ul class="navbar_list">
            <li>
                <a class="navbar_group mobile" href="index.php">
                    <i class="material-icons">home</i>
                </a>
            </li>
            <li>
                <a class="navbar_group" <!--href="changeCode.html"-->>
                <i class="material-icons">vpn_key</i>
                <p class="noMobile">Change PIN</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="?logout=">
                    <i class="material-icons">exit_to_app</i>
                    <p class="noMobile">Logout</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" <!--href="transactionHistory.html"-->>
                <i class="material-icons">monetization_on</i>
		<p>Solde : <?php echo $coffeeData['sumpaid'] - $coffeeData['sumdue'] ?></p>
                </a>
            </li>
        </ul>
    </div>
</div>

    <!-- START OF CONTENT -->
    <div id="contentContainer" class="container-fluid NoMP">
        <div class="row NoMP">
            <?php
            $qry = $db->prepare("SELECT name, description, img_url FROM achievements WHERE id = (?)");
            $qry->execute(array($achievementID));
            $row = $qry->fetch();
            ?>
            <p class="sub-title">Achievement Detail :</p>

            <div class="largeAchievementBlock">
	    <img class="achievementImgLarge" src="<?php
	    echo img_location; 
	    if ($row['img_url'] == "")
		    echo default_img;
	    else
		    echo $row['img_url'];
		  ?>">
                <div class="row NoMP">
                    <p class="largeAchievementTitle">
                        <?php
                        $qry = $db->prepare("SELECT achievements.name, achievements.description, achievement_difficulty.name as difficulty FROM achievements INNER JOIN achievement_difficulty ON achievement_difficulty.id = achievements.difficulty  WHERE achievements.id = (?)");
                        $qry->execute(array($achievementID));
                        $row = $qry->fetch();
			echo $row['name'];
                        ?>
                    </p>
                    <?php echo '<p class="difficultyTag unlocked' . $row['difficulty'] . '">' . $row['difficulty'] . '</p>'; ?>
                        <?php
                        $qry = $db->prepare('SELECT timestamp FROM achievement_user INNER JOIN users ON users.id = achievement_user.user WHERE achievement = (?) AND username = (?)');
                        $qry->execute(array($achievementID, $username));
                        $date = $qry->fetch();
                        if(isset($date['timestamp']))
                        {
                            $date = new DateTime($date['timestamp']);
                            echo '<p class="largeAchievementTimeCode">Unlocked on : ' . $date->format('d/m/Y') . ' at ' . $date->format('H:i') . '</p>';
                        }
                     ?>
                </div>
                <div class="row NoMP">
                    <p class="largeAchievementDesc">
                        <?php
                        echo $row['description'];
                        ?>
                    </p>
                </div>
            </div>

            <p class="sub-title">People who have unlocked this achievement :</p>

            <div class="largeBlock">
                <table class="table">
                    <thead class="tableHead">
                    <tr>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $qry = $db->prepare("SELECT initcap(replace(users.username, '.', ' ')) as username, achievement_user.timestamp FROM achievement_user RIGHT JOIN users on users.id = achievement_user.user WHERE achievement_user.achievement = (?) ORDER BY achievement_user.timestamp DESC");
                        $qry->execute(array($achievementID));
                        while ($row = $qry->fetch())
                        {
                            $date = new DateTime($row['timestamp']);
                            ?>
                            <tr>
                                <td><?php echo $row['username']; ?></td>
                                <td><?php echo $date->format('d/m/Y'); ?></td>
                                <td><?php echo $date->format('H:i'); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <!-- END OF CONTENT -->

</body>
</html>
