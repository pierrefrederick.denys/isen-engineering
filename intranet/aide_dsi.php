<?php /*
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
    require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
    require_once '/var/www/phpCAS/CAS/mustBeLogged.php'; */ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Panel Personnel IE</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="noindex,nofollow">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>
    <!--<script type="text/javascript" src='js/inscription.js'></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
      integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
      integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
    <link href="/css/prism.css" rel="stylesheet" />
    <script src="/js/prism.js"></script>
<link href="css/styleshfnet.css" rel="stylesheet" type="text/css" media="all"/>

</head>
<body>
<!--          header             -->
<div class="container">

    <!-- NAVBAR -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="https://isenengineering.fr"><img src="img/logo_ie.png" alt="Logo IE"
                                                                               title="logo_ie" width="45"
                                                                               class="img-responsive center-block"/></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="./index.php">Menu</a></li>
                    <li class="active"><a href="">Aide</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                </ul>
            </div>
        </div>
    </nav>

    <!-- CONTENU -->

    <div class="contenu">
        <h1 class="text-center">Aide</h1>

                <div class="container">
                    <div class="row">
                        <div class="col-lg-2" id="menu">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#SSH">SSH</a></li>
                                <li><a href="#LDAP">LDAP</a></li>
                                <li><a href="#certificats">Certificats</a></li>
                                <li><a href="#bitbucket">Bitbucket</a></li>
                                <li><a href="#BDD">BDD</a></li>
                                <li><a href="#depannage_isen">Depannage ISEN</a></li>

                            </ul>
                        </div>

                        <div class="col-lg-9">

                            <div id="SSH">
                                <h2>SSH</h2>
                                <h3>Connexion au serveur </h3>
                                <p>Pour se connecter en ssh au serveur, modifier le fichier <code class="language-bash">.ssh/config</code> afin d'ajouter
                                la configuration suivante  en modifiant  <code class="language-bash">{FICHIER_CLE_PRIVEE_IE}</code> par le fichier de votre clé privée
                                    et <code class="language-bash">{NOM_USER}</code> par le nom de votre utilisateur LDAP. </p>
                                <pre><code class="language-bash">host Engineering
	HostName 92.222.72.39
	Port 1996
	IdentityFile ~/.ssh/{FICHIER_CLE_PRIVEE_IE}
	User {NOM_USER}
	Cipher aes256-cbc</code></pre>

                                Pour se connecter  :  <code class="language-bash"> user@pc:~$ ssh Engineering</code>
                                <p>Le terminal attends ensuite que vous validiez la connexion via l'application DUO sécurité.</p>

                                <h3>Connexion sécurisée avec DUO</h3>
                                DUO est un service de 2fa permettant une authentification forte en plus du certificat et du mot de passe de l'utilisateur.

                                Sur le serveur, l'authentification est configurée dans le fichier <code class="language-bash">/etc/pam.d/common-auth</code> :

                                <pre><code class="language-bash">auth    sufficient                      pam_unix.so nullok_secure
auth    requisite                       pam_succeed_if.so uid >= 1000 quiet
auth    requisite                       pam_ldap.so use_first_pass
auth    sufficient                      pam_succeed_if.so user notingroup serveradmin quiet
auth    sufficient                      pam_duo.so
auth    requisite                       pam_deny.so</code></pre>




                                <h3>Connexion au Raspberry Pi </h3>
                                <p>Pour se connecter au raspberry pi, se connecter en tant que root au serveur puis : </p>
                                <code class="language-bash">root@isenengineering:~# ssh -i .ssh/coffee -p 2222 pi@127.0.0.1</code>




                           
                            </div>



                            <div id="LDAP">
                                <h2>LDAP</h2>
                                <p>Norme POSIX sur le LDAP </p>

                                <ul>
                                    <li><b>field givenName :</b> Firstname</li>
                                    <li><b>field sn :</b> surname (family name), sur le serveur : Lastname</li>
                                    <li><b>field cn :</b>  Common name (complete name), sur le serveur : Firstname Lastname</li>
                                </ul>

                                <h3>Ajout d'un utilisateur</h3>
                                <p>Dans le cas de l'inscription de nouveaux utilisateurs le script cherche l'UID le plus haut et inscrit l'utilisateur avec IDMAX+1</p>

                                <b>Inscription par lots :</b>
                                <p>Via ce <a href="https://ldap.isenengineering.fr/admin/?tab=inscription"> lien</a> il est possible d'inscripe plusieurs utilisateur en meme temps. <br>
                                L'inscription d'utilisateurs se fait au format prenom.nom:gid avec <i>prenom.nom</i> sans espaces ni accents, et gid le groupe principal de l'utilisateur. </p>

                            </div>

                            <div id="bitbucket">
                                <h2>Bitbucket</h2>

                                <p>Modifier le fichier <code class="language-bash">.ssh/config</code> de votre utilisateur avec la config suivante,
                                    en remplaçant <code class="language-bash">{NOM_CLE_SSH}</code> : par le nom du fichier de clé privée de votre pc.</p>
                                <pre><code class="language-bash">host bitbucket.isenengineering.fr
        HostName bitbucket.isenengineering.fr
        IdentityFile ~/.ssh/{NOM_CLE_SSH}
        Port 7999
        User git</code></pre>

                                <p> Sur le site Bitbucket <a href="https://bitbucket.isenengineering.fr/">lien</a> Cliquer sur gérer le compte
                                    <img src="img/aide_dsi/bitbucket1.png" alt="bitbucket1" width="100"/>
                                    puis <b>clés SSH</b> et enfin <b>Ajouter une clé</b> <img src="img/aide_dsi/bitbucket2.png" alt="bitbucket2" width="150"/>.</p>

                                <p>Copier-coller ensuite le contenu de votre fichier de clé publique (.pub) associée à la clé privée indiquée dans le fichier
                                    <code class="language-bash">.ssh/config</code> ci-dessus.</p>

                                <h3>Projet intranet</h3>
                                <p>La configuration suivante permet d'effectuer des modifications sur l'ensemble du site <a href="https://intranet.isenengineering.fr/">https://intranet.isenengineering.fr/</a> </p>
                                <ul>
                                    <li><a href="https://bitbucket.isenengineering.fr/projects/AS/repos/intranet/browse"> Lien du repos en ligne</a> </li>
                                    <li> <code class="language-bash"> git clone -b dev ssh://git@bitbucket.isenengineering.fr:7999/as/intranet.git</code>
                                        pour cloner le projet sur la branche <code class="language-bash">dev</code> et modifier l'intranet.</li>
                                    <li>Pour valider les modificiations, pusher sur la branche dev, puis :
                                        <a target="_blank" href="https://bitbucket.isenengineering.fr/projects/AS/repos/intranet/compare/commits?sourceBranch=refs%2Fheads%2Fdev">https://bitbucket.isenengineering.fr/projects/AS/repos/intranet/compare/commits?sourceBranch=refs%2Fheads%2Fdev</a>
                                        pour créer une pull request et ainsi mettre les modifications en production.</li>
                                </ul>

                                <h3>License JIRA/Bitbucket</h3>

                                <p> La license JIRA/Bitbucket est accodée par Atlassian pendant une période donnée. Elle est actuellement valable jusqu'au 27 févr. 2021<br>
                                  Lien pour vérifier l'état de la license : <a href="https://bitbucket.isenengineering.fr/admin/license">License</a>.   </p>


                            </div>


                            <div id="certificats">
                                <h2>Certificats</h2>
                                <p>Il est nécessaire de renouveler régulièrement les certificats des accès https du serveur.</p>
                                <p>Le certificat est délivré à l'aide de let's encrypt. </p>

                            </div>



                            <div id="BDD">
                                <h2>BDD</h2>
                                <p>Connexion aux BDD de l'IE avec Datagrip</p>
                                <img src="img/aide_dsi/datagrip1.png" alt="datagrip" title="datagrip" width="500" class="img-responsive center-block"/>
                                <img src="img/aide_dsi/datagrip2.png" alt="datagrip" title="datagrip" width="500" class="img-responsive center-block"/>
                            </div>







                            <div id="depannage_isen">
                                <h2>Dépannage réseau ISEN</h2>
                                <p>Sur le Raspberry Pi du local, un compte de dépannage est configuré afin d'autoriser Willy à dépanner la connexion réseau en cas
                                de besoin. Pour l'activer et le désactiver : </p>

                                <pre><code class="language-bash">usermod --lock --expiredate 1 willy
usermod --unlock --expiredate 'YYYY-MM-DD' willy</code></pre>
                                <pre><code class="language-bash">ssh willy@192.168.60.51
Password: izen</code></pre>
                            </div>


                           <!-- <div class="alert alert-warning" role="alert">
                                <p>carte </p>
                            </div>
                            <a target="_blank" class="btn btn-success" href="https://blog.isenengineering.fr/">bouton</a>-->


                        </div>
                    </div>
                </div>
    </div> <!-- /container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</body>
</html>

