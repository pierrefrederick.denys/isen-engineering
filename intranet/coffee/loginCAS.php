<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 24/01/18
 * Time: 23:14
 */


require_once '/var/www/phpCAS/CAS/mustBeLogged.php';

$_SESSION['username'] = phpCAS::getUser();
$_SESSION['logged'] = true;
$_SESSION['CASLogin'] = true;

header('Location: /coffee/index.php');