<?php

if(file_exists("/var/www/lib/bdd/DBFactory.php"))
    require_once "/var/www/lib/bdd/DBFactory.php";
else
    require_once $_SERVER['DOCUMENT_ROOT']."/conf/DBFactory.php";

define('img_location', './img/');
define('default_img', 'default.png');


use \IE\DBFactory as DBFactory;


function AddUserCoffee($username, $code)
{
    try
    {
        $db = DBFactory::createCoffeePDO();
        $qry = $db->prepare("SELECT COUNT(*) as count FROM users WHERE username=?");
        $qry->execute(array($username));
        $count = $qry->fetch();
        if (isset($count['count']) && $count['count'] == 1)
        {
            $qry = $db->prepare("SELECT COUNT(*) as count FROM code_change 
                    INNER JOIN users ON code_change.user_id = users.id 
                    WHERE users.username = ? AND 
                    timestamp > current_date - interval '7 days';");
            $qry->execute(array($username));
            $count = $qry->fetch();
            if ($count['count'] >= 3) return 'You can\'t change your code more than 3 times during one week.';

            $qry = $db->prepare("UPDATE users SET code=md5(?), blocked=true WHERE username=?;");
            $qry->execute(array($code, $username));
        } else
        {
            $qry = $db->prepare("INSERT INTO users (username, code, debts_allowed) VALUES(?, md5(?), false);");
            $qry->execute(array($username, $code));
        }
        return '';
    } catch (Exception $e)
    {
        //return $e->getMessage();//debug
        return 'Unknown error happened :(';
    }
}

function issetUser($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT COUNT(*) as count FROM users WHERE username=?");
    $qry->execute(array($username));
    $count = $qry->fetch();
    if (isset($count['count']) && $count['count'] == 1)
        return true;
    return false;
}

function addNewUser($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("INSERT INTO users (username, code, debts_allowed, disable) VALUES(?, md5((RANDOM()*10000)::TEXT), false, true);");
    $qry->execute(array($username));
}

function getUsernameFromCookie($cookie)
{
    try
    {
        $db = DBFactory::createCoffeePDO();
        $qry = $db->prepare("SELECT username FROM users WHERE cookie=?");
        $qry->execute(array($cookie));
        $user = $qry->fetch();
        if (isset($user['username']))
            return $user['username'];
        return null;
    } catch (Exception $e)
    {
        //return $e->getMessage();//debug
        return 'Unknown error happened :(';
    }
}

function storeCookieForUser($username, $cookie)
{
    try
    {
        $db = DBFactory::createCoffeePDO();
        $qry = $db->prepare("UPDATE users SET cookie = ? WHERE username = ?");
        $qry->execute(array($cookie, $username));
        return true;
    } catch (Exception $e)
    {
        //return $e->getMessage();//debug
        return 'Unknown error happened :(';
    }
}

function getStats($username)
{
    $stats = array();
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT id FROM users WHERE username=?;");
    $qry->execute(array($username));
    $userId = $qry->fetch(PDO::FETCH_ASSOC)['id'];

    $qry = $db->prepare("SELECT COUNT(*) as count, ROUND(SUM(0.5-coffee.price)::NUMERIC, 2) as saved FROM coffee WHERE user_id=?;");
    $qry->execute(array($userId));
    $res = $qry->fetch(PDO::FETCH_ASSOC);
    $stats['countCoffee'] = $res['count'];
    $stats['totalSaved'] = $res['saved'];
    if(empty($stats['totalSaved']))
        $stats['totalSaved'] = 0;

    $qry = $db->query("SELECT COUNT(*) as count FROM achievements;");
    $stats['countAchievements'] = $qry->fetch(PDO::FETCH_ASSOC)['count'];

    $qry = $db->prepare('SELECT COUNT(*) as count FROM achievement_user WHERE "user"=?;');
    $qry->execute(array($userId));
    $stats['countUnlockedAchievements'] = $qry->fetch(PDO::FETCH_ASSOC)['count'];

    $qry = $db->prepare('SELECT rnum as rank FROM (SELECT "user", COUNT(*) as count, row_number() OVER (ORDER BY COUNT(*) DESC, "user" ASC) as rnum FROM achievement_user GROUP BY "user") t WHERE "user" = ?;');
    $qry->execute(array($userId));
    $stats['rank'] = $qry->fetch(PDO::FETCH_ASSOC)['rank'];
    if(empty($stats['rank']))
        $stats['rank'] = 0;

    $qry = $db->query("SELECT COUNT(*) as count FROM users;");
    $stats['countUsers'] = $qry->fetch(PDO::FETCH_ASSOC)['count'];

    $qry = $db->prepare('SELECT ROUND((compte.sumpaid-compte.sumdue)::NUMERIC, 2) as solde FROM compte WHERE id = ?;');
    $qry->execute(array($userId));
    $stats['solde'] = $qry->fetch(PDO::FETCH_ASSOC)['solde'];


    return $stats;
}

function listAchievements($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT achievements.id, achievements.name as name, achievements.description as description, achi_user.user, achievement_difficulty.name as difficulty, achievements.img_url as img FROM achievements full outer join (SELECT achievement_user.achievement, achievement_user.user FROM achievement_user INNER JOIN users on users.id = achievement_user.user WHERE users.username = (?)) as achi_user on id = achi_user.achievement LEFT JOIN achievement_difficulty on achievement_difficulty.id = achievements.difficulty");
    $qry->execute(array($username));
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function getAchievement($id, $username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT achievements.name, achievements.description, achievements.img_url, achievement_difficulty.name as difficulty FROM achievements INNER JOIN achievement_difficulty ON achievement_difficulty.id = achievements.difficulty  WHERE achievements.id = (?)");
    $qry->execute(array($id));
    $res = $qry->fetch(PDO::FETCH_ASSOC);

    $qry = $db->prepare("SELECT id FROM users WHERE username=?;");
    $qry->execute(array($username));
    $userId = $qry->fetch(PDO::FETCH_ASSOC)['id'];

    $qry = $db->prepare('SELECT timestamp FROM achievement_user WHERE "user" = ? AND achievement = ?');
    $qry->execute(array($userId, $id));
    $res['timestamp'] = $qry->fetch(PDO::FETCH_ASSOC)['timestamp'];

    $qry = $db->prepare('SELECT INITCAP(TRANSLATE(username, \'.\', \' \')) as user, timestamp FROM achievement_user INNER JOIN users u ON achievement_user."user" = u.id WHERE achievement = ? ORDER BY timestamp DESC ');
    $qry->execute(array($id));
    $res['users'] = $qry->fetchAll(PDO::FETCH_ASSOC);

    return $res;
}

function getTransactions($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT id FROM users WHERE username=?;");
    $qry->execute(array($username));
    $userId = $qry->fetch(PDO::FETCH_ASSOC)['id'];

    $qry = $db->prepare("((SELECT (-price) as amount, timestamp, 'Coffee' as type FROM coffee WHERE user_id = ?) UNION ALL (SELECT money as amount, timestamp, 'Deposit' as type FROM paid WHERE user_id = ?)) ORDER BY timestamp DESC;");
    $qry->execute(array($userId,$userId));
    return $qry->fetchAll(PDO::FETCH_ASSOC);
}

function accountDisabled($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("SELECT disable FROM users WHERE username=?;");
    $qry->execute(array($username));
    return $qry->fetch(PDO::FETCH_ASSOC)['disable'];
}

function toggleAccount($username)
{
    $db = DBFactory::createCoffeePDO();
    $qry = $db->prepare("UPDATE users SET disable = NOT disable WHERE username=?;");
    $qry->execute(array($username));
}