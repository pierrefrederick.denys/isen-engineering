<?php

require_once './functions.php';
use \IE\DBFactory as DBFactory;

session_start();
if(!isset($_SESSION['logged']))
{
    header('Location: /coffee/login.php');
    exit(0);
}

if (!isset($_POST['code']))
{
    header('Location: /coffee/');
    exit(1);
}

$code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
$username = $_SESSION['username'];

if (empty($code) || empty($username))
    exit(1);

if (!preg_match('/^[0-9]{4,12}$/', $code)) {
    header('Location: /coffee/accountSettings.php?error=PIN code must contain between 4 and 12 digits.');
    exit(1);
}

try {
    $db = DBFactory::createCoffeePDO();
    
    $qry = $db->prepare("SELECT COUNT(*) as count FROM forbidden_passwords WHERE code = md5(?) OR code = ?;");
    $qry->execute(array($code, $code));
    $count = $qry->fetch();
    if(isset($count['count']) && $count['count'] > 0)
    {
        header('Location: /coffee/accountSettings.php?error=Code not allowed.');
        exit(1);
    }
    $qry = $db->prepare("SELECT COUNT(*) as count FROM users WHERE username=?");
    $qry->execute(array($username));
    $count = $qry->fetch();
    if(isset($count['count']) && $count['count'] == 1)
    {
        $qry = $db->prepare("SELECT COUNT(*) as count FROM code_change 
				INNER JOIN users ON code_change.user_id = users.id 
				WHERE users.username = ? AND 
				timestamp > current_date - interval '7 days';");
        $qry->execute(array($username));
        $count = $qry->fetch();
        if($count['count'] >= 3)
        {
            header('Location: /coffee/accountSettings.php?error=You cannot change your code more than 3 times a week.');
                exit(1);
        }
        $qry = $db->prepare("UPDATE users SET code=md5(?) WHERE username=?;");
        $qry->execute(array($code, $username));
    }

}
catch(PDOException $e) {
    if($e->getCode() == 23505)
    {
        try {
            $qry = $db->prepare("SELECT id, username FROM users WHERE code=md5(?)");
            $qry->execute(array($code));
            $user = $qry->fetch();
            //todo send email to the concerned person so the user $user['username']

            $qry = $db->prepare("UPDATE users SET code=? WHERE code=md5(?);");
            $qry->execute(array($user['id'], $code));

            $qry = $db->prepare("INSERT INTO forbidden_passwords (code) VALUES (md5(?));");
            $qry->execute(array($code));
            header('Location: /coffee/accountSettings.php?error=This password is not available, choose another one.');
        }
        catch(PDOException $e){
            print_r($e);
            exit(1);
            header('Location: /coffee/accountSettings.php?error=An error occurred. code 1' . $e->getMessage());
	    }
    }
    else
    {
	    print_r($e);
	    exit(1);
        header('Location: /coffee/accountSettings.php?error=An error occurred. code 2');
    }
    exit(1);
}

header('Location: /coffee/?success=Code saved.');
