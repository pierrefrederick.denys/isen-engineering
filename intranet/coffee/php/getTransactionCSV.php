<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 14/02/18
 * Time: 23:18
 */

session_start();
if(!isset($_SESSION['logged']))
    header('Location: login.php');

require_once 'functions.php';

$username = $_SESSION['username'];
$list = getTransactions($username);
$csv = 'Date;Time;Operation;Amount'.PHP_EOL;
foreach ($list as $tran)
{
    $date = new DateTime($tran['timestamp']);
    $csv .= '"'.$date->format('d/m/Y').'";"'.$date->format('H:m:i').'";"'
        .str_replace('"', '\'', $tran['type']).'";"'.$tran['amount'].'€"'.PHP_EOL;
}


header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename=isen-engineering_operation.csv');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: no-cache');
header('Content-Length: ' . strlen($csv));

echo $csv;
