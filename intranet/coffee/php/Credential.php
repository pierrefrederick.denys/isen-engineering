<?php

namespace IE;

class Credential
{
    private $curl;
    function __construct()
    {
        $this->curl = curl_init("https://ent-toulon.isen.fr/j_spring_security_check");

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0 Iceweasel/58.1.0');
        curl_setopt($this->curl, CURLOPT_POST, 2);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
    }

    function Test($username, $password)
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query(array('j_username' => $username, 'j_password' => $password)));
        curl_exec($this->curl);
        $out = curl_getinfo($this->curl);

        if (preg_match('/erreur.html/', $out['redirect_url']))
            return false;
        return true;
    }
}
