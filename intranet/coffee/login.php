<?php
require_once './php/functions.php';
require_once './php/Credential.php';

use \IE\Credential as Credential;

session_start();
if(isset($_SESSION['logged']))
    header('Location: /coffee/index.php');

$error = '';
if(isset($_COOKIE['coffee_cookie']))
{
    $cookie = filter_input(INPUT_COOKIE, 'coffee_cookie', FILTER_SANITIZE_STRING);
    $username = getUsernameFromCookie($cookie);
    if($username != null)
    {
        $_SESSION['username'] = $username;
        $_SESSION['logged'] = true;
        $_SESSION['CASLogin'] = false;
        header('Location: /coffee/index.php');
        exit(0);
    }
    else
        setcookie( "coffee_cookie", '', 0, '/coffee', $_SERVER['SERVER_NAME'], true );
}

if(isset($_POST['username']) && isset($_POST['password']))
{
    $username = strtolower(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING));
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $remember = (isset($_POST['memorize'])) ? true : false;

    $credential = new Credential();
    if($credential->Test($username, $password))
    {
        if(!issetUser($username))
            addNewUser($username);

        $_SESSION['username'] = $username;
        $_SESSION['logged'] = true;
        $_SESSION['CASLogin'] = false;
        if($remember)
        {
            try
            {
                $cookie = bin2hex(random_bytes(64));
                if(setcookie( "coffee_cookie", $cookie, strtotime( '+30 days' ), '/coffee', $_SERVER['SERVER_NAME'], true ))
                    storeCookieForUser($username, $cookie);
            } catch (Exception $e)
            {

            }
        }
        header('Location: /coffee/index.php');
        exit(0);
    }
    else
        $error = 'Wpass';
        $errorMsg = '<div class="errorMsg Wpass"><i class="material-icons">warning</i><p> Login / Password invalid !</p></div>';

}


?>
<!DOCTYPE html>
<html>
<title>Achievements</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-3.3.0.js"></script>
<script src="js/popper_1.12.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/customized/ui.css">
<link rel="stylesheet" href="css/customized/responsive.css">

<style>
    /* Page specific code for background image */
    html {
        margin: 0;
        padding: 0;
        background: url(img/login_background.jpg) no-repeat center fixed;
        -webkit-background-size: cover;
        background-size: cover;
    }

    body {
        background-color: transparent !important;
    }
</style>

<body>

<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group" href="index.php">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title">Coffee Achievements</p>
        </a>
    </div>
</div>

<!-- START OF CONTENT -->
<div id="contentContainer" class="container-fluid NoMP">
    <div class="row NoMP">

        <div class="sub-title txtWhite mt20 mb20"><i class="material-icons">fingerprint</i>
            <p>Identification required</p></div>

    </div>
    <div class="row NoMP">
        <div class="loginBlock <?php if(isset($error)){echo $error;} ?>">
            <form action="login.php" method="POST">
                <?php if(isset($errorMsg)){echo $errorMsg;} ?>
                <label for="username">Username : </label>
                <input id="username" name="username" type="text" autofocus required>
                <label for="password">Password : </label>
                <input id="password" name="password" type="password" required>
                <div class="row NoMP">
                    <div class="checkboxBlock"><label id="memorizeLabel" for="memorize">Memorize </label><input id="memorize" name="memorize" type="checkbox"></div>
                </div>
                <input type="submit" id="submitButton" value="Connect">

                <a class="vButton" id="CasButton" href="loginCAS.php">
                    <i class="material-icons">vpn_key</i>
                    <p> CAS </p>
                </a>
            </form>
        </div>
    </div>
</div>
<!-- END OF CONTENT -->

</body>
</html>
