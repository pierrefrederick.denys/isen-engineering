<?php

session_start();
if(!isset($_SESSION['logged']))
    header('Location: login.php');

require_once 'php/functions.php';

use \IE\DBFactory as DBFactory;

try
{
    $db = DBFactory::createCoffeePDO();
} catch (Exception $e)
{
    echo 'Database connection failed';
    exit(1);
}
$achievementID = filter_input ( INPUT_GET , "id", FILTER_SANITIZE_NUMBER_INT);
$username = $_SESSION['username'];
$achievement = getAchievement($achievementID, $username);
$stats = getStats($username);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Achievements</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-3.3.0.js"></script>
    <script src="js/popper_1.12.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/customized/ui.css">
    <link rel="stylesheet" href="css/customized/achievements.css">
    <link rel="stylesheet" href="css/customized/responsive.css">
</head>
<body>
<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group noMobile" href="index.php">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title noMobile">Coffee Achievements</p>
        </a>
        <ul class="navbar_list">
            <li>
                <a class="navbar_group mobile" href="index.php">
                    <i class="material-icons">home</i>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="accountSettings.php">
                <i class="material-icons">settings</i>
                <p class="noMobile">Settings</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="?logout=">
                    <i class="material-icons">exit_to_app</i>
                    <p class="noMobile">Logout</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="transactionHistory.php">
                <i class="material-icons">monetization_on</i>
                <p>Solde : <?php echo $stats['solde']; ?>€</p>
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- START OF CONTENT -->
<div id="contentContainer" class="container-fluid NoMP">
    <div class="row NoMP">
        <p class="sub-title">Achievement Detail :</p>

        <div class="largeAchievementBlock">
            <img class="achievementImgLarge" src="<?php
            echo img_location;
            if ($achievement['img_url'] == "")
                echo default_img;
            else
                echo $achievement['img_url'];
            ?>">
            <div class="row NoMP">
                <p class="largeAchievementTitle">
                    <?php
                    echo $achievement['name'];
                    ?>
                </p>
                <?php echo '<p class="difficultyTag unlocked' . $achievement['difficulty'] . '">' . $achievement['difficulty'] . '</p>'; ?>
                <?php

                if(!empty($achievement['timestamp']))
                {
                    $date = new DateTime($achievement['timestamp']);
                    echo '<p class="largeAchievementTimeCode">Unlocked on : ' . $date->format('d/m/Y') . '</p>';
                }
                ?>
            </div>
            <div class="row NoMP">
                <p class="largeAchievementDesc">
                    <?php
                    echo $achievement['description'];
                    ?>
                </p>
            </div>
        </div>
        <?php if(!empty($achievement['users']))
        { ?>
            <p class="sub-title">People who have unlocked this achievement :</p>

            <div class="largeBlock">
                <table class="table">
                    <thead class="tableHead">
                    <tr>
                        <th>Name</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($achievement['users'] as $user)
                    {
                        $date = new DateTime($user['timestamp']);
                        ?>
                        <tr>
                            <td><?php echo $user['user']; ?></td>
                            <td><?php echo $date->format('d/m/Y'); ?></td>
                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <?php
        }
        else
        {
            ?>
            <p class="sub-title">Nobody has unlocked it yet</p>
            <?php
        }
        ?>

    </div>
</div>
<!-- END OF CONTENT -->

</body>
</html>
