<?php

session_start();
if(!isset($_SESSION['logged']))
{
    header('Location: login.php');
    exit(0);
}

require_once './php/functions.php';

if(isset($_GET['error']))
{
    $error = 'Wpass';
    $errorMsg = '<div class="errorMsg"><i class="material-icons">warning</i><p> '
        . filter_input(INPUT_GET, 'error', FILTER_SANITIZE_STRING) .
        '</p></div>';
}

$username = $_SESSION['username'];
if(isset($_GET['toggle']))
    toggleAccount($username);
$disabled = accountDisabled($username);

?>
<!DOCTYPE html>
<html>
<title>Achievements</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-3.3.0.js"></script>
<script src="js/popper_1.12.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/customized/ui.css">
<link rel="stylesheet" href="css/customized/responsive.css">

<body>

<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group" href="index.php">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title">Coffee Achievements</p>
        </a>
    </div>
</div>

<!-- START OF CONTENT -->
<div id="contentContainer" class="container-fluid NoMP">
    <div class="row NoMP">

        <div class="sub-title mt20 mb20"><i class="material-icons">vpn_key</i>
            <p>Change PIN</p></div>
    </div>

    <div class="row NoMP">
        <div class="loginBlock <?php if(isset($error)){echo $error;} ?>">
            <form action="php/updateCode.php" method="post">
                <?php if(isset($errorMsg)){echo $errorMsg;} ?>
                <label for="code">New PIN : </label>
                <input id="code" type="password" name="code" autofocus required autocomplete="off">
                <input type="submit" id="submitButton" value="Change">
            </form>
        </div>

    </div>

    <div class="row NoMP">
        <div class="sub-title mt20 mb20"><i class="material-icons">check_circle</i>
            <p>Account status</p></div>
    </div>

    <div class="row NoMP">
        <div class="loginBlock">
            <?php if(isset($disabled) && !$disabled){ ?>
            <a class="vButton deactivate" id="accountStatusButton" href="?toggle">
                    <i class="material-icons">pause_circle_filled</i>
                    <p>Disable account</p>
                </a>
            <?php } else if(isset($disabled) && $disabled){?>
            <a class="vButton activate" id="accountStatusButton" href="?toggle">
                    <i class="material-icons">play_circle_filled</i>
                    <p>Enable account</p>
                </a>
            <?php } ?>
        </div>

    </div>

    <a class="vButton" id="returnButton" href="index.php">
        <i class="material-icons">arrow_back</i>
        <p> Retour</p>
    </a>

</div>
<!-- END OF CONTENT -->

</body>
</html>
