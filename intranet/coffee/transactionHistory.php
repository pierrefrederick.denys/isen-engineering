<?php

session_start();
if(!isset($_SESSION['logged']))
    header('Location: login.php');

require_once 'php/functions.php';

use \IE\DBFactory as DBFactory;

try
{
    $db = DBFactory::createCoffeePDO();
} catch (Exception $e)
{
    echo 'Database connection failed';
    exit(1);
}

$username = $_SESSION['username'];

$stats = getStats($username);

?>

<!DOCTYPE html>
<html>
<title>Achievements</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-3.3.0.js"></script>
<script src="js/popper_1.12.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/customized/ui.css">
<link rel="stylesheet" href="css/customized/responsive.css">
<body>

<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group noMobile" href="index.php">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title noMobile">Coffee Achievements</p>
        </a>
        <ul class="navbar_list">
            <li>
                <a class="navbar_group mobile" href="index.php">
                    <i class="material-icons">home</i>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="accountSettings.php">
                    <i class="material-icons">settings</i>
                    <p class="noMobile">Settings</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="/coffee/?logout">
                    <i class="material-icons">exit_to_app</i>
                    <p class="noMobile">Logout</p>
                </a>
            </li>
            <li>
                <a class="navbar_group" href="transactionHistory.php">
                    <i class="material-icons">monetization_on</i>
                    <p>Solde : <?php echo $stats['solde']; ?>€</p>
                </a>
            </li>
        </ul>
    </div>
</div>


<!-- START OF CONTENT -->
<div id="contentContainer" class="container-fluid NoMP">
    <div class="row NoMP">

        <div class="sub-title mt20"><i class="material-icons">monetization_on</i>
            <p> Transaction History :</p>
        </div>

    </div>
    <div class="row NoMP">

        <a class="vButton" id="ExcelExportButton" href="php/getTransactionCSV.php">
            <i class="material-icons">file_download</i>
            <p> Export to Excel </p>
        </a>

    </div>
    <div class="row NoMP">
        <div class="largeBlock">
            <table class="table">
                <thead class="tableHead">
                <tr>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Operation</th>
                    <th>Amount</th>
                    <!--<th>Happy Hour</th>-->
                </tr>
                </thead>
                <tbody>
                <?php
                $list = getTransactions($username);
                foreach ($list as $tran)
                {
                    $date = new DateTime($tran['timestamp']);
                    ?>
                    <tr>
                        <td><?php echo $date->format('d/m/Y'); ?></td>
                        <td><?php echo $date->format('H:m:i'); ?></td>
                        <td><?php echo $tran['type']; ?></td>
                        <td><?php echo $tran['amount']; ?>€</td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>

    </div>
</div>
<!-- END OF CONTENT -->

</body>
</html>
