<!DOCTYPE html>
<html>
<title>Achievements</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery-3.3.0.js"></script>
<script src="js/popper_1.12.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/customized/ui.css">
<link rel="stylesheet" href="css/customized/responsive.css">

<body>

<div id="navbarContainer" class="container">
    <div class="row NoMP">
        <a class="navbar_group" href="index.html">
            <i class="material-icons iconTitle">local_cafe</i>
            <p class="title">Coffee Achievements</p>
        </a>
    </div>
</div>

<!-- START OF CONTENT -->
<div id="contentContainer" class="container-fluid NoMP">
    <div class="row NoMP">
        <p class="sub-title mt20 mb20"><i class="material-icons">fingerprint</i>Login mode</p>
    </div>
    <div class="row NoMP">
        <div class="loginBlock">
            <p>Are you a member of ISEN Engineering ?</p>
            <a class="vButton YNb green" href="loginCAS.php"><i class="material-icons">check</i>
                <p>Yes</p></a>
            <a class="vButton YNb red" href="login.php"><i class="material-icons">clear</i>
                <p>No</p></a>
        </div>

    </div>
</div>
<!-- END OF CONTENT -->

</body>
</html>
