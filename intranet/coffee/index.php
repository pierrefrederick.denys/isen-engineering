<?php

require_once './php/functions.php';

session_start();
if(isset($_GET['logout']))
{
    $cas = $_SESSION['CASLogin'];

    setcookie( "coffee_cookie", '', 0, '/coffee', $_SERVER['SERVER_NAME'], true );
    unset($_SESSION['logged']);
    session_destroy();

    if($cas)
        header('Location: https://cas.isenengineering.fr/cas/logout');
    else
        header('Location: /coffee/login.php');
    exit();
}
if(!isset($_SESSION['logged']))
{
    header('Location: /coffee/login.php');
    exit(0);
}


$username = $_SESSION['username'];
$stats = getStats($username);

$error = filter_input(INPUT_GET, 'error', FILTER_SANITIZE_STRING);
$success = filter_input(INPUT_GET, 'success', FILTER_SANITIZE_STRING);


?>
<!DOCTYPE html>
<html>
<head>
    <title>Achievements</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="js/jquery-3.3.0.js"></script>
    <script src="js/popper_1.12.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="css/customized/ui.css">
    <link rel="stylesheet" href="css/customized/achievements.css">
    <link rel="stylesheet" href="css/customized/responsive.css">
</head>
<body>


    <div id="navbarContainer" class="container">
        <div class="row NoMP">
            <a class="navbar_group noMobile" href="index.php">
                <i class="material-icons iconTitle">local_cafe</i>
                <p class="title noMobile">Coffee Achievements</p>
            </a>
            <ul class="navbar_list">
                <li>
                    <a class="navbar_group mobile" href="index.php">
                        <i class="material-icons">home</i>
                    </a>
                </li>
                <li>
                    <a class="navbar_group" href="accountSettings.php">
                        <i class="material-icons">settings</i>
                        <p class="noMobile">Settings</p>
                    </a>
                </li>
                <li>
                    <a class="navbar_group" href="?logout=">
                        <i class="material-icons">exit_to_app</i>
                        <p class="noMobile">Logout</p>
                    </a>
                </li>
                <li>
                    <a class="navbar_group" href="transactionHistory.php">
                        <i class="material-icons">monetization_on</i>
                        <p>Solde : <?php echo $stats['solde']; ?>€</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>


    <!-- START OF CONTENT -->
    <div id="contentContainer" class="container-fluid NoMP">
        <?php if(!empty($error)) {; ?>
        <div class="row NoMP">
            <div class="largeMessage red">
                <i class="material-icons">warning</i>
                <p><?php echo $error; ?></p>
            </div>
        </div>
        <?php } ?>
        <?php if(!empty($success)) {; ?>
        <div class="row NoMP">
            <div class="largeMessage green">
                <i class="material-icons">check</i>
                <p><?php echo $success; ?></p>
            </div>
        </div>
        <?php } ?>

        <div class="row NoMP">
            <div class="sub-title"><i class="material-icons">face</i>
                <p>
                    <?php
                        echo ucwords(str_replace('.', ' ', $username));
                    ?>
                </p>
            </div>
        </div>

        <div class="row NoMP">

            <div class="stats_block">
                <ul class="stat_list">
                    <li class="stat_group">
                        <i class="material-icons">local_cafe</i>
                        <p class="stat_key">Total coffee : </p>
                        <p class="stat_value"><?php echo $stats['countCoffee']; ?></p>
                    </li>
                    <li class="stat_group">
                        <i class="material-icons">stars</i>
                        <p class="stat_key">Unlocked achievements : </p>
			<p class="stat_value">
			<?php
            echo $stats['countUnlockedAchievements'] . '/' . $stats['countAchievements'];
			?>
			</p>
                    </li>
                    <li class="stat_group">
                        <i class="material-icons">flag</i>
                        <p class="stat_key">Percentage completed : </p>
                        <p class="stat_value"><?php echo ($stats['countUnlockedAchievements'] == 0) ? '0' : round(($stats['countUnlockedAchievements'] / $stats['countAchievements'])*100, 0); ?>%</p>
                    </li>
                    <li class="stat_group">
                        <i class="material-icons">monetization_on</i>
                        <p class="stat_key">Total saved : </p>
                        <p class="stat_value"><?php echo $stats['totalSaved']; ?>€</p>
                    </li>
                    <li class="stat_group">
                        <i class="material-icons">insert_chart</i>
                        <p class="stat_key">Ranking : </p>
                        <p class="stat_value"><?php echo $stats['rank'] . '/' . $stats['countUsers']; ?></p>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row NoMP mt20">
            <?php
            $achievements = listAchievements($username);
            foreach ($achievements as $row)
            {
                ?>
                <a href="achievementDetail.php?id=<?php echo $row['id'];?>" class="achievementBlock 
                <?php
                if($row['user'] =='')
                {
                    echo "locked ";
                }
                else
                {
                    echo "unlocked$row[difficulty]";
                }
                ?>
                ">
		<img class="achievementImg" src="<?php
						echo img_location;
						if($row['img'] == '')
						{
							echo default_img;
						}
						else
						{
							echo $row['img'];
						}
						?>">
                <p class="achievementTitle"><?php echo $row['name'];?></p>
                <p class="achievementDesc"><?php echo $row['description'];?></p>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
    <!-- END OF CONTENT -->

</body>
</html>

