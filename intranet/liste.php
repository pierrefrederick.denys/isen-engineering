<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';
require_once './php/functions.php';

$users = ListeUsers();
$usersGroup = ListeGroupes();
usort($users, 'CompareParNom');
$group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
?>
<table class="table table-striped table-fluid">
	<thead>
		<th><b>Nom</b></td>
		<th><b>Prénom</b></td>
		<?php if($group != 'anciens') { ?>
		<th><b>Promo</b></td>
		<?php } ?>
		<th class="col-xs-1"><b>Contact</b></td>
	</thead>
	<?php
	foreach($users as $user)
	{
		if(empty($group) && in_array('anciens', $usersGroup[$user['uid']]) && count($usersGroup[$user['uid']]) == 1)
			continue;
	
		if(!in_array($group, $usersGroup[$user['uid']]) && !empty($group))
			continue;
		if($group != 'bureau' && !empty($group) && in_array('bureau', $usersGroup[$user['uid']]))
			echo '<tr class="success">';
		else
			echo '<tr>';
		?>
			<td><?php echo $user['sn']; ?></td>
			<td><?php echo $user['givenname']; ?></td>
			<?php if($group != 'anciens') { ?>
				<td><?php echo $user['promo']; ?></td>
			<?php } ?>
			<td><?php echo '<a href="mailto:'.$user['mail'].'"><span class="glyphicon glyphicon-envelope"></span></a>'; ?></td>
		</tr>
		<?php
	}
	?>	
</table>

