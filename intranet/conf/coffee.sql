--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: Coffee; Type: DATABASE; Schema: -; Owner: coffeeadmin
--



CREATE USER coffeeadmin WITH PASSWORD 'coffee';
CREATE USER coffee WITH PASSWORD 'coffee';


CREATE DATABASE "Coffee" WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE "Coffee" OWNER TO coffeeadmin;

\connect "Coffee"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

--
-- Name: notification_type; Type: TYPE; Schema: public; Owner: coffeeadmin
--

CREATE TYPE notification_type AS ENUM (
    'coffee-ok',
    'coffee-no-money',
    'coffee-money-warning',
    'coffee-locked',
    'achievement'
);


ALTER TYPE notification_type OWNER TO coffeeadmin;

--
-- Name: add_price_on_insert_coffee(); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION add_price_on_insert_coffee() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      NEW.price = (SELECT value FROM parameters WHERE name = 'unit_price');
      RETURN NEW;
    END;
$$;


ALTER FUNCTION public.add_price_on_insert_coffee() OWNER TO coffeeadmin;

--
-- Name: check_achievements(integer, integer); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION check_achievements(p_achievement_type integer, p_user_id integer DEFAULT 0) RETURNS boolean
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
      result INT;
      resultList TEXT := '{}';
      ach achievements%ROWTYPE;
      curs CURSOR FOR(
                        SELECT *
                        FROM achievements
                        WHERE "type" = p_achievement_type
                          AND achievements.id NOT IN (
                                        SELECT achievement_user.achievement
                                        FROM achievement_user
                                        WHERE achievement_user."user" = p_user_id
                        )
      );
BEGIN
      IF(p_achievement_type = (SELECT achievement_type.id FROM achievement_type WHERE achievement_type."description" = 'each'))
      THEN
        -- ___________________________________ EACH TIME ______________________________________
        IF(p_user_id = 0)
        THEN
          RETURN FALSE;
        END IF;

        FOR ach in curs
        LOOP
          EXECUTE ach.command
            INTO result
            USING p_user_id;

            IF(result = 1)
            THEN
              INSERT INTO achievement_user("user", "achievement") VALUES (p_user_id, ach.id);

              INSERT INTO notifications("content", "type", "delay") VALUES ((SELECT TRANSLATE(json_agg(t)::TEXT, '[]', '') FROM
                (SELECT
                   ach.name as title,
                   ach.description,
                   ach.img_url ,
                   (SELECT achievement_difficulty.name FROM achievement_difficulty WHERE id = ach.difficulty) as difficulty,
                  (SELECT initcap(left(users.username, strpos(users.username, '.')-1)) FROM users WHERE id = p_user_id) as "user"
                 ) t), 'achievement', 20);

            END IF;
        END LOOP;
        -- ___________________________________ EACH TIME ______________________________________


      ELSEIF((SELECT 1 FROM achievement_type WHERE achievement_type.id = p_achievement_type AND achievement_type."description" IN('weekly', 'daily', 'yearly', 'monthly')) = 1)
      THEN
        -- ___________________________________ EACH WEEK,DAY,YEAR  ______________________________________

        FOR ach in curs
        LOOP
          EXECUTE 'SELECT array_agg(user_id) FROM (' || ach.command || ') as t'
            INTO resultList;

          INSERT INTO achievement_user
            SELECT * FROM (SELECT unnest(string_to_array(translate(resultList, '{}', ''):: TEXT, ',')):: INT AS "user", ach.id AS "achievement") t
              WHERE NOT EXISTS(
                  SELECT "user", "achievement"
                  FROM achievement_user
                  WHERE
                    t."user"=achievement_user."user" AND
                    t."achievement"=achievement_user."achievement");


        END LOOP;
        -- ___________________________________ EACH WEEK,DAY,YEAR ______________________________________


      END IF;

      RETURN TRUE;
END;
$$;


ALTER FUNCTION public.check_achievements(p_achievement_type integer, p_user_id integer) OWNER TO coffeeadmin;

--
-- Name: insert_coffee(); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION insert_coffee() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE solde REAL;
    DECLARE blocked BOOLEAN;
    DECLARE debts_allowed BOOLEAN;
    DECLARE firstname TEXT;
    BEGIN
      IF (NEW.price IS NOT NULL)
        THEN
        RETURN NEW;
      END IF;
      IF (NEW.user_id IS NULL)
        THEN
        RETURN NULL;
      END IF;
      firstname = (SELECT initcap(left(users.username, strpos(users.username, '.')-1)) FROM users WHERE id = NEW.user_id);
      blocked = (SELECT users.blocked FROM users WHERE id = NEW.user_id);
      IF(blocked)
        THEN
        INSERT INTO events("event_type", "user") VALUES ((SELECT id FROM event_type WHERE name = 'locked_user_attempt'), NEW.user_id);
        INSERT INTO notifications("content", "type") VALUES ((SELECT TRANSLATE(json_agg(t)::TEXT, '[]', '') FROM (SELECT firstname as "user", 0 as solde) t), 'coffee-locked');
        RETURN NULL;
      END IF;

      NEW.price = (SELECT value FROM parameters WHERE name = 'unit_price');
      solde = (SELECT sumpaid-sumdue FROM compte WHERE id = NEW.user_id);
      debts_allowed = (SELECT users.debts_allowed FROM users WHERE id = NEW.user_id);

      IF(solde > NEW.price OR debts_allowed)
      THEN
        IF(solde < NEW.price)
          THEN
          INSERT INTO notifications("content", "type") VALUES ((SELECT TRANSLATE(json_agg(t)::TEXT, '[]', '') FROM (SELECT firstname as "user", ROUND((solde-NEW.price)::NUMERIC, 2)::text as solde) t), 'coffee-money-warning');
        ELSE
          INSERT INTO notifications("content", "type") VALUES ((SELECT TRANSLATE(json_agg(t)::TEXT, '[]', '') FROM (SELECT firstname as "user", ROUND((solde-NEW.price)::NUMERIC, 2)::text as solde) t), 'coffee-ok');
        END IF;
        RETURN NEW;
      ELSE
        INSERT INTO notifications("content", "type") VALUES ((SELECT TRANSLATE(json_agg(t)::TEXT, '[]', '') FROM (SELECT firstname as "user", ROUND(solde::NUMERIC, 2)::text as solde) t), 'coffee-no-money');
        RETURN NULL;
      END IF;
      RETURN NULL;
    END;
$$;


ALTER FUNCTION public.insert_coffee() OWNER TO coffeeadmin;

--
-- Name: log_code_change(); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION log_code_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF OLD.code != NEW.code THEN
            INSERT INTO code_change (code, user_id) VALUES (OLD.code, NEW.id);
        END IF;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.log_code_change() OWNER TO coffeeadmin;

--
-- Name: log_happy_hour(); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION log_happy_hour() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF (OLD.name = 'happy_hour' AND NEW.value = 'true' AND NEW.value != OLD.value)
        THEN
            INSERT INTO events (event_type) VALUES ((SELECT id FROM event_type WHERE "name"='happy_hour_up'));
        ELSEIF (OLD.name = 'happy_hour' AND NEW.value = 'false' AND NEW.value != OLD.value)
        THEN
            INSERT INTO events (event_type) VALUES ((SELECT id FROM event_type WHERE "name"='happy_hour_down'));
        END IF;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.log_happy_hour() OWNER TO coffeeadmin;

--
-- Name: test_achievements(); Type: FUNCTION; Schema: public; Owner: coffeeadmin
--

CREATE FUNCTION test_achievements() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
      PERFORM check_achievements(1, NEW.user_id);
      RETURN NEW;
    END;
$$;


ALTER FUNCTION public.test_achievements() OWNER TO coffeeadmin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: achievement_difficulty; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE achievement_difficulty (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE achievement_difficulty OWNER TO coffeeadmin;

--
-- Name: achievement_difficulty_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE achievement_difficulty_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE achievement_difficulty_id_seq OWNER TO coffeeadmin;

--
-- Name: achievement_difficulty_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE achievement_difficulty_id_seq OWNED BY achievement_difficulty.id;


--
-- Name: achievement_type; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE achievement_type (
    id integer NOT NULL,
    description text
);


ALTER TABLE achievement_type OWNER TO coffeeadmin;

--
-- Name: achievement_type_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE achievement_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE achievement_type_id_seq OWNER TO coffeeadmin;

--
-- Name: achievement_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE achievement_type_id_seq OWNED BY achievement_type.id;


--
-- Name: achievement_user; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE achievement_user (
    "user" integer NOT NULL,
    achievement integer NOT NULL,
    "timestamp" timestamp without time zone DEFAULT timezone('Europe/Paris'::text, now()) NOT NULL
);


ALTER TABLE achievement_user OWNER TO coffeeadmin;

--
-- Name: achievements; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE achievements (
    id integer NOT NULL,
    name text,
    description text,
    command text,
    type integer,
    img_url text,
    difficulty integer NOT NULL
);


ALTER TABLE achievements OWNER TO coffeeadmin;

--
-- Name: achievements_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE achievements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE achievements_id_seq OWNER TO coffeeadmin;

--
-- Name: achievements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE achievements_id_seq OWNED BY achievements.id;


--
-- Name: code_change; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE code_change (
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    code text,
    user_id integer NOT NULL
);


ALTER TABLE code_change OWNER TO coffeeadmin;

--
-- Name: coffee; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE coffee (
    id integer NOT NULL,
    user_id integer NOT NULL,
    "timestamp" timestamp without time zone DEFAULT timezone('Europe/Paris'::text, now()) NOT NULL,
    price real NOT NULL
);


ALTER TABLE coffee OWNER TO coffeeadmin;

--
-- Name: coffee_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE coffee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coffee_id_seq OWNER TO coffeeadmin;

--
-- Name: coffee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE coffee_id_seq OWNED BY coffee.id;


--
-- Name: compte; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE compte (
    id integer,
    username text,
    debts_allowed boolean,
    blocked boolean,
    countcoffee bigint,
    sumdue real,
    sumpaid real
);

ALTER TABLE ONLY compte REPLICA IDENTITY NOTHING;


ALTER TABLE compte OWNER TO coffeeadmin;

--
-- Name: event_type; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE event_type (
    id integer NOT NULL,
    description text,
    name character varying(30) NOT NULL
);


ALTER TABLE event_type OWNER TO coffeeadmin;

--
-- Name: events; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE events (
    "timestamp" timestamp without time zone DEFAULT timezone('Europe/Paris'::text, now()) NOT NULL,
    event_type integer NOT NULL,
    "user" integer
);


ALTER TABLE events OWNER TO coffeeadmin;

--
-- Name: COLUMN events."user"; Type: COMMENT; Schema: public; Owner: coffeeadmin
--

COMMENT ON COLUMN events."user" IS 'User id, if relevant';


--
-- Name: forbidden_passwords; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE forbidden_passwords (
    code text NOT NULL
);


ALTER TABLE forbidden_passwords OWNER TO coffeeadmin;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE notifications (
    id text DEFAULT md5((random())::text) NOT NULL,
    content text,
    type notification_type,
    "timestamp" timestamp without time zone DEFAULT timezone('Europe/Paris'::text, now()),
    delay integer DEFAULT 30000 NOT NULL
);


ALTER TABLE notifications OWNER TO coffeeadmin;

--
-- Name: paid; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE paid (
    id integer NOT NULL,
    user_id integer NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    money real NOT NULL
);


ALTER TABLE paid OWNER TO coffeeadmin;

--
-- Name: paid_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE paid_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE paid_id_seq OWNER TO coffeeadmin;

--
-- Name: paid_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE paid_id_seq OWNED BY paid.id;


--
-- Name: parameters; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE parameters (
    name text NOT NULL,
    value text,
    description text
);


ALTER TABLE parameters OWNER TO coffeeadmin;

--
-- Name: users; Type: TABLE; Schema: public; Owner: coffeeadmin
--

CREATE TABLE users (
    id integer NOT NULL,
    username text NOT NULL,
    code text NOT NULL,
    blocked boolean DEFAULT false NOT NULL,
    debts_allowed boolean DEFAULT false NOT NULL,
    disable boolean DEFAULT false NOT NULL,
    cookie text
);


ALTER TABLE users OWNER TO coffeeadmin;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: coffeeadmin
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO coffeeadmin;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: coffeeadmin
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: achievement_difficulty id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_difficulty ALTER COLUMN id SET DEFAULT nextval('achievement_difficulty_id_seq'::regclass);


--
-- Name: achievement_type id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_type ALTER COLUMN id SET DEFAULT nextval('achievement_type_id_seq'::regclass);


--
-- Name: achievements id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievements ALTER COLUMN id SET DEFAULT nextval('achievements_id_seq'::regclass);


--
-- Name: coffee id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY coffee ALTER COLUMN id SET DEFAULT nextval('coffee_id_seq'::regclass);


--
-- Name: paid id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY paid ALTER COLUMN id SET DEFAULT nextval('paid_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: achievement_difficulty achievement_difficulty_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_difficulty
    ADD CONSTRAINT achievement_difficulty_pkey PRIMARY KEY (id);


--
-- Name: achievement_type achievement_type_id_pk; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_type
    ADD CONSTRAINT achievement_type_id_pk PRIMARY KEY (id);


--
-- Name: achievement_user achievement_user_user_achievement_pk; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_user
    ADD CONSTRAINT achievement_user_user_achievement_pk PRIMARY KEY ("user", achievement);


--
-- Name: achievements achievements_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievements
    ADD CONSTRAINT achievements_pkey PRIMARY KEY (id);


--
-- Name: code_change code_change_timestamp_pk; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY code_change
    ADD CONSTRAINT code_change_timestamp_pk PRIMARY KEY ("timestamp");


--
-- Name: coffee coffee_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY coffee
    ADD CONSTRAINT coffee_pkey PRIMARY KEY (id);


--
-- Name: event_type event_type_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY event_type
    ADD CONSTRAINT event_type_pkey PRIMARY KEY (id);


--
-- Name: events events_timestamp_event_type_pk; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_timestamp_event_type_pk PRIMARY KEY ("timestamp", event_type);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: paid paid_id_pk; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY paid
    ADD CONSTRAINT paid_id_pk PRIMARY KEY (id);


--
-- Name: parameters parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: Users_username_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX "Users_username_uindex" ON users USING btree (username);


--
-- Name: achievement_difficulty_name_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX achievement_difficulty_name_uindex ON achievement_difficulty USING btree (name);


--
-- Name: coffee_timestamp_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX coffee_timestamp_uindex ON coffee USING btree ("timestamp");


--
-- Name: event_type_name_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX event_type_name_uindex ON event_type USING btree (name);


--
-- Name: users_code_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX users_code_uindex ON users USING btree (code);


--
-- Name: users_cookie_uindex; Type: INDEX; Schema: public; Owner: coffeeadmin
--

CREATE UNIQUE INDEX users_cookie_uindex ON users USING btree (cookie);


--
-- Name: compte _RETURN; Type: RULE; Schema: public; Owner: coffeeadmin
--

CREATE RULE "_RETURN" AS
    ON SELECT TO compte DO INSTEAD  SELECT t1.id,
    t1.username,
    t1.debts_allowed,
    t1.blocked,
    t1.countcoffee,
    COALESCE(t1.sumdue, (0)::real) AS sumdue,
    COALESCE(t2.somme, (0)::real) AS sumpaid
   FROM (( SELECT users.id,
            users.username,
            users.debts_allowed,
            users.blocked,
            count(coffee.id) AS countcoffee,
            sum(coffee.price) AS sumdue
           FROM (users
             LEFT JOIN coffee ON ((users.id = coffee.user_id)))
          GROUP BY users.username, users.id) t1
     LEFT JOIN ( SELECT paid.user_id,
            sum(paid.money) AS somme
           FROM paid
          GROUP BY paid.user_id) t2 ON ((t1.id = t2.user_id)));


--
-- Name: coffee insert_price; Type: TRIGGER; Schema: public; Owner: coffeeadmin
--

CREATE TRIGGER insert_price BEFORE INSERT ON coffee FOR EACH ROW EXECUTE PROCEDURE insert_coffee();


--
-- Name: users log_code_change; Type: TRIGGER; Schema: public; Owner: coffeeadmin
--

CREATE TRIGGER log_code_change BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE log_code_change();


--
-- Name: parameters log_happy_hour; Type: TRIGGER; Schema: public; Owner: coffeeadmin
--

CREATE TRIGGER log_happy_hour AFTER UPDATE ON parameters FOR EACH ROW EXECUTE PROCEDURE log_happy_hour();


--
-- Name: coffee test_achievements_trigger; Type: TRIGGER; Schema: public; Owner: coffeeadmin
--

CREATE TRIGGER test_achievements_trigger AFTER INSERT ON coffee FOR EACH ROW EXECUTE PROCEDURE test_achievements();


--
-- Name: achievement_user achievement_user_achievement_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_user
    ADD CONSTRAINT achievement_user_achievement_fk FOREIGN KEY (achievement) REFERENCES achievements(id);


--
-- Name: achievement_user achievement_user_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievement_user
    ADD CONSTRAINT achievement_user_user_fk FOREIGN KEY ("user") REFERENCES users(id);


--
-- Name: achievements achievements_difficulty_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievements
    ADD CONSTRAINT achievements_difficulty_fk FOREIGN KEY (difficulty) REFERENCES achievement_difficulty(id);


--
-- Name: achievements achievements_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY achievements
    ADD CONSTRAINT achievements_type_fk FOREIGN KEY (type) REFERENCES achievement_type(id);


--
-- Name: code_change code_change_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY code_change
    ADD CONSTRAINT code_change_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: coffee coffee_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY coffee
    ADD CONSTRAINT coffee_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: events events_event_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_event_type_fkey FOREIGN KEY (event_type) REFERENCES event_type(id);


--
-- Name: events events_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_user_fk FOREIGN KEY ("user") REFERENCES users(id);


--
-- Name: paid paid_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: coffeeadmin
--

ALTER TABLE ONLY paid
    ADD CONSTRAINT paid_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO coffeeadmin;
GRANT USAGE ON SCHEMA public TO coffee;


--
-- Name: add_price_on_insert_coffee(); Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT ALL ON FUNCTION add_price_on_insert_coffee() TO coffee;


--
-- Name: check_achievements(integer, integer); Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT ALL ON FUNCTION check_achievements(p_achievement_type integer, p_user_id integer) TO coffee;


--
-- Name: insert_coffee(); Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT ALL ON FUNCTION insert_coffee() TO coffee;


--
-- Name: log_happy_hour(); Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT ALL ON FUNCTION log_happy_hour() TO coffee;


--
-- Name: test_achievements(); Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT ALL ON FUNCTION test_achievements() TO coffee;


--
-- Name: achievement_difficulty; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE achievement_difficulty TO coffee;


--
-- Name: achievement_type; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE achievement_type TO coffee;


--
-- Name: achievement_user; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT ON TABLE achievement_user TO coffee;


--
-- Name: achievements; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE achievements TO coffee;


--
-- Name: code_change; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT ON TABLE code_change TO coffee;


--
-- Name: coffee; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT ON TABLE coffee TO coffee;


--
-- Name: coffee_id_seq; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,UPDATE ON SEQUENCE coffee_id_seq TO coffee;


--
-- Name: compte; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE compte TO coffee;


--
-- Name: event_type; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE event_type TO coffee;


--
-- Name: events; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT INSERT ON TABLE events TO coffee;


--
-- Name: forbidden_passwords; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT ON TABLE forbidden_passwords TO coffee;


--
-- Name: notifications; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT,DELETE ON TABLE notifications TO coffee;


--
-- Name: paid; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE paid TO coffee;


--
-- Name: parameters; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT ON TABLE parameters TO coffee;


--
-- Name: users; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,INSERT,UPDATE ON TABLE users TO coffee;


--
-- Name: users_id_seq; Type: ACL; Schema: public; Owner: coffeeadmin
--

GRANT SELECT,UPDATE ON SEQUENCE users_id_seq TO coffee;


--
-- Data for Name: achievement_difficulty; Type: TABLE DATA; Schema: public; Owner: coffeeadmin
--

COPY achievement_difficulty (id, name) FROM stdin;
4	Legendary
3	Hard
2	Medium
1	Easy
\.


--
-- Name: achievement_difficulty_id_seq; Type: SEQUENCE SET; Schema: public; Owner: coffeeadmin
--

SELECT pg_catalog.setval('achievement_difficulty_id_seq', 4, true);


--
-- Data for Name: achievement_type; Type: TABLE DATA; Schema: public; Owner: coffeeadmin
--

COPY achievement_type (id, description) FROM stdin;
1	each
2	daily
3	weekly
4	monthly
5	yearly
6	never
7	each_achievement
\.


--
-- Name: achievement_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: coffeeadmin
--

SELECT pg_catalog.setval('achievement_type_id_seq', 7, true);




--
-- Data for Name: achievements; Type: TABLE DATA; Schema: public; Owner: coffeeadmin
--


COPY achievements (id, name, description, command, type, img_url, difficulty) FROM stdin;
35	Happy new year !	Premier café de l'année civile	\N	6	\N	3
20	Sevrage	Ne pas prendre de café pendant 3 mois	\N	6	\N	1
25	Corruption	Offrir un café à l'admin	\N	6	\N	4
30	SNCF	Prendre le café dans les 5 mins après l'happy hour	\N	6	\N	1
4	Apprenti	Leader du classement sur la semaine	\N	6	\N	2
18	Le début d'une grande aventure !	Prendre son premier café	\N	6	\N	1
2	Coffee Master	Etre en tête du classement annuel en fin d'année	\N	6	\N	4
23	Précision	Prendre le 500ème café de l'année	\N	6	\N	4
22	On time	Prendre le 100ème café de l'année	\N	6	\N	4
24	Sniper	Prendre le 1000ème café de l'année	\N	6	\N	4
34	PFD	Prendre un café avant 7h40 et après 19h le même jour	\N	6	\N	4
14	Communiste	Prends 3 cafés en moins de 2 min	\N	6	\N	2
12	04h20	Prendre un café à 16h20	\N	6	\N	1
31	Pas de chance !	Prendre son café 5min avant l'happy hour	\N	6	\N	1
13	First !!!	Premier café du mois	\N	6	First.png	1
\.



--
-- Name: achievements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: coffeeadmin
--

SELECT pg_catalog.setval('achievements_id_seq', 36, true);





--
-- Data for Name: parameters; Type: TABLE DATA; Schema: public; Owner: coffeeadmin
--

COPY parameters (name, value, description) FROM stdin;
last_filling	2018-01-23 20:05:52.038032+01	Compteur de cafés
max	37	Nombre de dosettes max
happy_hour	false	Happy Hour
message	 	Message de l'écran
unit_price	0.15	Prix unitaire
maintenance	false	Mode maintenance
\.










--
-- PostgreSQL database dump complete
--



