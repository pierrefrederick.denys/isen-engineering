<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 24/01/18
 * Time: 18:09
 */

namespace IE;


class DBFactory
{

    public static function createCoffeePDO()
    {
        try
        {
            $options[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;
            $BDD = new \PDO('pgsql:host=' . 'localhost' . ';dbname=' . 'Coffee', 'coffee', 'coffee', $options);
        }
        catch (\PDOException $e)
        {
            echo 'Database connection failed';
            exit(1);
        }
        return $BDD;
    }

    public static function createCoffeeAdminPDO()
    {
        try
        {
            $options[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;
            $BDD = new \PDO('pgsql:host=' . 'localhost' . ';dbname=' . 'Coffee', 'coffeeadmin', 'coffee', $options);
        }
        catch (\PDOException $e)
        {
            echo 'Database connection failed';
            exit(1);
        }
        return $BDD;
    }

}
