<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';
require_once 'functions.php';
use \IE\DBFactory as DBFactory;
define('__FABSITE__', '/var/www/clubs/isenopensource/fablab/');

if($IS_ADMIN || $IS_UPKEEP || in_array('isenopensource', ListeGroupes()[phpCAS::getUser()]))
{
	$file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);
	$filename = filter_input(INPUT_GET, 'filename', FILTER_SANITIZE_STRING);
	$url = __FABSITE__.'uploads/'.$file;
	if (file_exists($url)) 
	{
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		print(base64_decode(file_get_contents($url)));
		exit;
	}

}
else
{
	header('HTTP/1.0 401 Unauthorized');
	header('Location: /401');
	exit(0);
}

