<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';
require_once './functions.php';
require_once '/var/www/lib/config.php';
use \IE\DBFactory as DBFactory;

if (!isset($_POST['code']))
    exit(1);

$code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
$username = phpCAS::getUser();

if (empty($code) || empty($username))
    exit(1);

$listeUsers = ListeUsers();
$find = false;

foreach($listeUsers as $ligne)
{
    if($ligne['uid'] == $username)
    {
        $find = true;
        break;
    }
}

if(!$find)
    exit(1);

if (!preg_match('/^[0-9]{4,12}$/', $code)) {
    header('Location: /?error=Le code doit contenir entre 4 et 12 chiffres.');
    exit(1);
}

try {
    $db = DBFactory::createCoffeePDO();
    
    $qry = $db->prepare("SELECT COUNT(*) as count FROM forbidden_passwords WHERE code = md5(?) OR code = ?;");
    $qry->execute(array($code, $code));
    $count = $qry->fetch();
    if(isset($count['count']) && $count['count'] > 0)
    {
        header('Location: /?error=Erreur, ce mot de passe n\'est pas disponible, choisisez en un autre');
        exit(1);
    }
    $qry = $db->prepare("SELECT COUNT(*) as count FROM users WHERE username=?");
    $qry->execute(array($username));
    $count = $qry->fetch();
    if(isset($count['count']) && $count['count'] == 1)
    {
        $qry = $db->prepare("SELECT COUNT(*) as count FROM code_change 
				INNER JOIN users ON code_change.user_id = users.id 
				WHERE users.username = ? AND 
				timestamp > current_date - interval '7 days';");
	$qry->execute(array($username));
	$count = $qry->fetch();
	if($count['count'] >= 3)
	{
	    header('Location: /?error=Vous ne pouvez pas changer votre code plus de 3 fois en une semaine.');
            exit(1);
	}
	$qry = $db->prepare("UPDATE users SET code=md5(?) WHERE username=?;");
	$qry->execute(array($code, $username));
  
    }
    else
    {
    	$qry = $db->prepare("INSERT INTO users (username, code) VALUES(?, md5(?));");
	$qry->execute(array($username, $code));
    }
}
catch(PDOException $e) {
    if($e->getCode() == 23505)
    {
	try {
	    $qry = $db->prepare("SELECT id, username FROM users WHERE code=md5(?)");
	    $qry->execute(array($code));
	    $user = $qry->fetch();
	    //todo send email to the concerned person so the user $user['username']
	    
	    $qry = $db->prepare("UPDATE users SET code=? WHERE code=md5(?);");
	    $qry->execute(array($user['id'], $code));

	    $qry = $db->prepare("INSERT INTO forbidden_passwords (code) VALUES (md5(?));");
	    $qry->execute(array($code));
	    header('Location: /?error=Erreur, ce mot de passe n\'est pas disponible, choisisez en un autre');
	}
        catch(PDOException $e){
	    print_r($e);
	    exit(1);
            header('Location: /?error=Une erreur inconnue est survenue. code 1' . $e->getMessage());   
	}
    }
    else
    {
	print_r($e);
	exit(1);
        header('Location: /?error=Une erreur inconnue est survenue. code 2');
    }
    exit(1);
}

header('Location: /');
