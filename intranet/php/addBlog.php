<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';
require_once 'functions.php';
require_once 'htmlpurifier/library/HTMLPurifier.auto.php';


if(isset($_POST['title']) && isset($_POST['subtitle']) && isset($_POST['club']) && isset($_POST['editor']))
{
	$club = filter_input(INPUT_POST, 'club', FILTER_SANITIZE_STRING);
	$title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
	$subtitle = filter_input(INPUT_POST, 'subtitle', FILTER_SANITIZE_STRING);
	$publish = isset($_POST['publish']);
	$purifier = new HTMLPurifier();
	$editor = $purifier->purify($_POST['editor']);
	$groups = ListeGroupes()[phpCAS::getUser()];
	if(in_array($club, $groups))
	{
		$bdd = ConnexionDB();
		$clubBlog = BlogClub($bdd, $club);
		if($clubBlog != null)
		{
			$qry = $bdd->prepare('INSERT INTO blog.article (author, club, content, title, subtitle, published) VALUES(?, ?, ?, ?, ?, ?)');
			var_dump($publish);
			$qry->execute(array(ucwords(str_replace('.', ' ', phpCAS::getUser())), $clubBlog['id'], $editor, $title, $subtitle, ($publish ? 1 : 0)));
			header('Location: /?posted');
			exit(0);
		}
	}
}

header('Location: /?error=Une erreur est survenue.');
