<?php
define('__GIDISENENGINEERING__', '500');

require_once "/var/www/lib/config.php";
require_once "/var/www/lib/bdd/DBFactory.php";

use \IE\DBFactory as DBFactory;


function ListeUsers()
{
	$ldap = ldap_connect(ldaphost, ldapport);
	

	$basedn = 'ou=users,dc=isenengineering,dc=fr';
	$justthese = array('*');

	$sr = ldap_list($ldap, $basedn, 'uid=*', $justthese);

	$info = ldap_get_entries($ldap, $sr);

	$result = array();

	for ($i=0; $i < $info['count']; $i++) {
		$entry = array();
		foreach($info[$i] as $key=>$value)
			$entry[$key]=$info[$i][$key][0];
		$result[$i] = $entry;
	}

	return $result;
}

function ListeGUID()
{
	$ldap = ldap_connect(ldaphost, ldapport);
	

	$basedn = 'ou=clubs,ou=groups,dc=isenengineering,dc=fr';
	$justthese = array('cn', 'gidNumber', 'memberUid');

	$sr = ldap_list($ldap, $basedn, '(!(gidNumber='.__GIDISENENGINEERING__.'))', $justthese);

	$info = ldap_get_entries($ldap, $sr);

	$result = array();

	for ($i=0; $i < $info['count']; $i++) {
		$entry = array();
		$entry['cn'] = $info[$i]['cn'][0];
		$entry['guid'] = $info[$i]['gidnumber'][0];
		if(isset($info[$i]['memberuid']))
			$entry['count'] = $info[$i]['memberuid']['count'];
		else
			$entry['count'] = 0;
		$result[$i] = $entry;
	}

	return $result;
}

function ListeGroupes()
{

	$ldap = ldap_connect(ldaphost, ldapport);


	$basedn = 'ou=clubs,ou=groups,dc=isenengineering,dc=fr';
	$justthese = array('cn', 'memberUid');

	$sr = ldap_list($ldap, $basedn, 'cn=*', $justthese);

	$info = ldap_get_entries($ldap, $sr);


	$result = array();

	for ($i=0; $i < $info['count']; $i++)
	{
		if(!isset($info[$i]['memberuid']))
			continue;
		foreach($info[$i]['memberuid'] as $uid)
		{
			if(!isset($result[$uid]))
				$result[$uid] = array();
			array_push($result[$uid], $info[$i]['cn'][0]);
		}	

	}
	
	$basedn = 'ou=groups,dc=isenengineering,dc=fr';
	$justthese = array('cn', 'memberUid');

	$sr = ldap_list($ldap, $basedn, 'cn=formermember', $justthese);
	$info = ldap_get_entries($ldap, $sr);

	for ($i=0; $i < $info['count']; $i++)
	{
		foreach($info[$i]['memberuid'] as $uid)
		{
			if(!isset($result[$uid]))
				$result[$uid] = array();
			array_push($result[$uid], 'anciens');
		}	

	}


	return $result;
}

function CompareParNom($a, $b)
{
	$keySort = 'sn';

	if($a[$keySort] == $b[$keySort])
		return 0;

	return ($a[$keySort] > $b[$keySort]) ? 1 : -1;
}
function CoffeeBDD()
{
    return DBFactory::createCoffeePDO();
}


function CoffeeData(PDO $db)
{
    $qry = $db->prepare("SELECT * FROM compte WHERE username=?;");
    $qry->execute(array(phpCAS::getUser()));

    return $qry->fetch();

}

function CoffeeParam(PDO $db)
{
    $qry = $db->query("SELECT * FROM parameters;");

    $result = $qry->fetchAll(PDO::FETCH_ASSOC);
    $params = array();
    foreach($result as $r)
    {
	    $params[$r['name']] = $r['value'];
    }

    return $params;

}

function ConnexionDB()
{
    return DBFactory::createServiceAdminPDO();
}

function BlogClub(PDO $BDD, $cn)
{
	$requette = $BDD->prepare('SELECT * FROM blog.club WHERE cn = ?;');
	$requette->execute(array($cn));
	return $requette->fetch(PDO::FETCH_ASSOC);

}
function BlogsCN(PDO $BDD)
{
	$requette = $BDD->query('SELECT cn FROM blog.club;');
	return $requette->fetchAll(PDO::FETCH_COLUMN);

}
function BlogArticles(PDO $BDD, $cn)
{
	$requette = $BDD->prepare('SELECT article.id, article.author, article.title, article.published FROM blog.article INNER JOIN blog.club ON article.club = club.id WHERE club.cn = ? ORDER BY id DESC LIMIT 10');
	$requette->execute(array($cn));
	return $requette->fetchAll(PDO::FETCH_ASSOC);
}
