<?php
if(preg_match('/^dev\./',$_SERVER['SERVER_NAME']) === 1)
	require_once '/var/www/phpCAS/CAS/mustBeAdmin.php';
else
	require_once '/var/www/phpCAS/CAS/mustBeLogged.php';
require_once './php/functions.php';
use \IE\DBFactory as DBFactory;

if($IS_ADMIN || $IS_UPKEEP || in_array('isenopensource', ListeGroupes()[phpCAS::getUser()]) || in_array('bureau', ListeGroupes()[phpCAS::getUser()]))
{
    $BDD = DBFactory::createOpenSourcePDO();
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
	$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
	$qry = $BDD->prepare('UPDATE fablab.demande SET done = NOT done WHERE id = ?');
	$qry->execute(array($id));

    }
    $qry = $BDD->query('SELECT demande.id, nom, prenom, mail, date, done, filename, uri, message, type_travail.name as travail_name, type_demande.name as demande_name FROM fablab.demande INNER JOIN fablab.type_demande ON demande.id_type = type_demande.id INNER JOIN fablab.type_travail ON demande.id_travail = type_travail.id ORDER BY done ASC, date DESC;');
    $liste = $qry->fetchAll(PDO::FETCH_ASSOC);
    ?>
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <td> </td>
                <td><b>Nom</b></td>
                <td><b>Prénom</b></td>
                <td><b>Date</b></td>
                <td><b>Projet</b></td>
                <td><b>Travail</b></td>
                <td><b>Détails</b></td>
                <td><b><span class="glyphicon glyphicon-ok"></span></b></td>
             </thead>
	    <?php
	    foreach($liste as $row)
	    {
		?>
		    <tr class="accordion-toggle<?php if($row['done']) echo ' success'; ?>">
		    	<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['nom']; ?></td>
			<td><?php echo $row['prenom']; ?></td>
			<td><?php $date = new DateTime($row['date']); echo $date->format('d/m/Y à H:i'); ?></td>
			<td><?php echo $row['demande_name']; ?></td>
			<td><?php echo $row['travail_name']; ?></td>
			<td>
			    <a href="#" title="Message" class="btn btn-link btn-sm" data-toggle="collapse" data-target="#fablab<?php echo $row['id']; ?>" >Plus</a>
			    <a target="_blank" href="./php/getFabFile.php?filename=<?php echo $row['filename']; ?>&file=<?php echo $row['uri']; ?>" onclick="alert('Attention : Ce fichier pourrait être dangeureux');" class="btn btn-link btn-sm" >Fichier</a>
			    <a href="mailto:<?php echo $row['mail']; ?>" class="btn btn-link btn-sm" >Mail</a>
			</td>
			<td><input type="checkbox" onclick="refreshFablab(<?php echo $row['id']; ?>);" <?php if($row['done']) echo 'checked'; ?>/></td>
		    </tr>
		    <tr class="bg-info">
			<td class="hiddenRow"></td>
			<td colspan="7" class="hiddenRow"><div class="accordion-body collapse fabAccordion" id="fablab<?php echo $row['id']; ?>"><?php echo $row['message']; ?><br/><br/><span class="glyphicon glyphicon-envelope"></span><a href="mailto:<?php echo $row['mail']; ?>" class="brn btn-link btn-sm"><?php echo $row['mail']; ?></a></div></td>
		    </tr>
		<?php
	    }
	    ?>
	</table>
    </div>
<?php

}
else
{
	header('HTTP/1.0 401 Unauthorized');
	header('Location: /401');
	exit(0);
}

